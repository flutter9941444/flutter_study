import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(title: 'Flutter Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _namesTextEditingController =
      TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  List<String> lst = List.empty(growable: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          foregroundColor: Colors.white,
          title: Text(widget.title),
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: _namesTextEditingController,
              focusNode: _nameFocusNode,
              decoration: InputDecoration(
                hintText: "Enter Name",
                border: InputBorder.none,
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: const BorderSide(color: Colors.blue),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(
                    color: Colors.pink,
                  ),
                ),
              ),
              cursorColor: Colors.amber,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.phone,
              onChanged: (value) {
                print("Value = $value");
              },
              onSubmitted: (value) {
                print("DATA SUBMITTED = $value");
              },
            ),
            const SizedBox(height: 5),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  var temp = _namesTextEditingController.text;
                  lst.add(temp);
                });
              },
              child: const Icon(Icons.add),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(
                child: ListView.builder(
                  
                    itemCount: lst.length,
                    itemBuilder: ((context, index) {
                      return Center(
                          child: Container(
                              height: 20,
                              margin: const EdgeInsets.all(10),
                              color: const Color.fromARGB(255, 82, 232, 187),
                              child: Text(
                                lst[index],
                              )));
                    })))
          ],
        ));
  }
}
