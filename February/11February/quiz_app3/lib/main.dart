import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class QuestionModel {
  final String? question;
  final List<String>? options;
  final int? correct;
  const QuestionModel({this.question, this.options, this.correct});
}

class _QuizAppState extends State {
  //variables
  int selected = -1;
  int queNo = 0;
  bool questionScreen = false;
  bool isHome = true;
  int result = 0;
  String msg = "";

  //questionlist
  List allQuestions = [
    const QuestionModel(
        question:
            "What is the key configuration file used when building a Flutter project?",
        options: ["pubspec.xml", "pubspec.yaml", "config.html", "root.xml"],
        correct: 1),
    const QuestionModel(
        question:
            "Who developed the Flutter Framework and continues to maintain it today?",
        options: ["Facebook", "Microsoft", "Google", "Oracle"],
        correct: 2),
    const QuestionModel(
      question:
          "Which programming language is used to build Flutter applications?",
      options: ["Kotlin", "Java", "Go", "Dart"],
      correct: 3,
    ),
    const QuestionModel(
        question: "How many types of widgets are there in Flutter?",
        options: ["2", "4", "6", "8+"],
        correct: 0),
    const QuestionModel(
        question:
            "Which function will return the widgets attached to the screen as a root of the widget tree to be rendered on screen?",
        options: ["main()", "runApp()", "container()", "root()"],
        correct: 1)
  ];

  //methods
  MaterialStateProperty<Color> giveColor(int index) {
    if (selected != -1) {
      if (index == allQuestions[queNo].correct) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (index == selected) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(Colors.deepPurple);
      }
    }
    return const MaterialStatePropertyAll(Colors.deepPurple);
  }

  void validatePage() {
    if (selected == -1) {
      msg = "Please Select an option !!!";
    } else {
      msg = "";
      if (selected == allQuestions[queNo].correct) {
        result++;
      }
      if (queNo == allQuestions.length - 1) {
        questionScreen = false;

        selected = -1;
      } else {
        queNo++;
        selected = -1;
      }
    }
    setState(() {});
  }

  SizedBox printResult() {
    if (result == 0) {
      return SizedBox(
          width: 300,
          child: Column(
            children: [
              Image.asset(
                "assets/sad.png",
                height: 150,
                width: 150,
              ),
              const SizedBox(height: 10),
              const Text("OPPS...",
                  style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.italic)),
              const SizedBox(height: 5),
              Text("You Have Scored $result out of ${allQuestions.length}",
                  style: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.italic)),
              const SizedBox(height: 5),
              const Text("Better Luck Next Time..",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.italic)),
            ],
          ));
    } else {
      return SizedBox(
        width: 350,
        child: Column(
          children: [
            Image.asset(
              "assets/cup.jpg",
              height: 300,
              width: 500,
            ),
            const Text(
              "Congratulations !!",
              style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 5),
            Text(
              "You Have Scored $result out of ${allQuestions.length}",
              style: const TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.italic),
            )
          ],
        ),
      );
    }
  }

  Scaffold isQuestionScreen() {
    if (isHome) {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.deepPurple,
            title: const Text("Quiz App",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic)),
            centerTitle: true,
          ),
          body: Column(
            children: [
              const Row(),
              const SizedBox(height: 30),
              Image.asset(
                "assets/home.jpeg",
                height: 250,
                width: 500,
              ),
              //const SizedBox(height: 10),

              Image.asset(
                "assets/welcome.png",
                height: 130,
                width: 250,
              ),

              /*
              const Text(
                "Welcome...",
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: 40,
                    fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                height: 10,
              ),*/
              const Text(
                "Quiz Topic : Flutter",
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 12,
              ),
              Text(
                "Question Count = ${allQuestions.length}",
                style:
                    const TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 40,
              ),
              SizedBox(
                height: 60,
                width: 200,
                child: ElevatedButton(
                    style: const ButtonStyle(
                        backgroundColor:
                            MaterialStatePropertyAll(Colors.deepPurple)),
                    onPressed: () {
                      questionScreen = true;
                      isHome = false;
                      setState(() {});
                    },
                    child: const Text("Start Quiz",
                        style: TextStyle(fontSize: 30))),
              )
            ],
          ));
    } else {
      if (questionScreen) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.deepPurple,
            title: const Text("Quiz App",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic)),
            centerTitle: true,
          ),
          floatingActionButton: FloatingActionButton(
              backgroundColor: Colors.deepPurple,
              onPressed: () {
                validatePage();
              },
              child: const Icon(Icons.forward)),
          body: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Questions : ${queNo + 1} / ${allQuestions.length}",
                      style: const TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.w700,
                      ))
                ],
              ),
              const SizedBox(
                height: 40,
              ),
              Row(children: [
                const SizedBox(
                  width: 18,
                ),
                SizedBox(
                    width: 350,
                    child: Text(allQuestions[queNo].question,
                        style: const TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w500,
                        ))),
              ]),
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 50,
                width: 320,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(0),
                  ),
                  onPressed: () {
                    if (selected == -1) {
                      selected = 0;
                      msg = "";
                    }
                    setState(() {});
                  },
                  child: Text("A. ${allQuestions[queNo].options[0]}",
                      style: const TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500)),
                ),
              ),
              const SizedBox(height: 13),
              SizedBox(
                height: 50,
                width: 320,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(1),
                  ),
                  onPressed: () {
                    if (selected == -1) {
                      selected = 1;
                      msg = "";
                    }
                    setState(() {});
                  },
                  child: Text("B. ${allQuestions[queNo].options[1]}",
                      style: const TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500)),
                ),
              ),
              const SizedBox(height: 13),
              SizedBox(
                height: 50,
                width: 320,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(2),
                  ),
                  onPressed: () {
                    if (selected == -1) {
                      selected = 2;
                      msg = "";
                    }
                    setState(() {});
                  },
                  child: Text("C. ${allQuestions[queNo].options[2]}",
                      style: const TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500)),
                ),
              ),
              const SizedBox(height: 13),
              SizedBox(
                height: 50,
                width: 320,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(3),
                  ),
                  onPressed: () {
                    if (selected == -1) {
                      selected = 3;
                      msg = "";
                    }
                    setState(() {});
                  },
                  child: Text("D. ${allQuestions[queNo].options[3]}",
                      style: const TextStyle(
                          fontSize: 25, fontWeight: FontWeight.w500)),
                ),
              ),
              const SizedBox(height: 30),
              Text(msg,
                  style: const TextStyle(
                      color: Colors.red,
                      fontSize: 27,
                      fontWeight: FontWeight.w600)),
            ],
          ),
        );
      } else {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.deepPurple,
              title: const Text("Quiz App",
                  style: TextStyle(
                      fontSize: 35,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.italic)),
              centerTitle: true,
            ),
            body: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    printResult(),
                    const SizedBox(height: 30),
                    SizedBox(
                      height: 50,
                      child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              questionScreen = true;
                              queNo = 0;
                              result = 0;
                            });
                          },
                          style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Colors.deepPurple)),
                          child: const Text(
                            "Reset",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w400),
                          )),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                        height: 50,
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              isHome = true;
                              questionScreen = false;
                              result = 0;
                              queNo = 0;
                            });
                          },
                          style: const ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(Colors.deepPurple)),
                          child: const Text(
                            "Back To Home",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w400),
                          ),
                        ))
                  ],
                )
              ],
            ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
