import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ToDo(),
    );
  }
}

class ToDo extends StatefulWidget {
  const ToDo({super.key});

  @override
  State<ToDo> createState() => _ToDoState();
}

class _ToDoState extends State<ToDo> {
  List<Color> colorList = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "To-do list",
          style: TextStyle(
              fontWeight: FontWeight.w700,
              height: 18,
              fontSize: 26,
              color: Colors.white),
        ),
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
      ),
      body: Padding(
        padding:
            //const EdgeInsets.only(top: 30, left: 15, right: 15, bottom: 40),
            const EdgeInsets.symmetric(horizontal: 15, vertical: 35),
        child: ListView.builder(
          itemCount: 8,
          itemBuilder: (context, index) {
            return Column(children: [
              Container(
                  height: 112,
                  width: 330,
                  decoration: BoxDecoration(
                    color: colorList[index % colorList.length],
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 52,
                                width: 52,
                                //padding: const EdgeInsets.all(10),
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle),
                                child: Image.asset("images/Group 42.png"),
                              ),
                              const Expanded(
                                // Expanded automatically
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 15,
                                      width: 243,
                                      child: Text(
                                        "Lorem Ipsum is simply setting industry.",
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    SizedBox(
                                      height: 44,
                                      width: 243,
                                      child: Text(
                                        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          const Row(
                            children: [
                              Text(
                                "10 July 2023",
                                style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(132, 132, 132, 1),
                                ),
                              ),
                              Spacer(
                                  // flex: 50,   Nahi dila tari chalta
                                  ),
                              SizedBox(
                                height: 13,
                                width: 13,
                                child: Icon(Icons.edit),
                              ),
                              SizedBox(
                                width: 19,
                              ),
                              SizedBox(
                                height: 13,
                                width: 10,
                                child: Icon(Icons.delete),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                            ],
                          ),
                        ]),
                  )),
              const SizedBox(
                height: 20,
              )
            ]);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Container(
          height: 46,
          width: 46,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(0, 139, 148, 1),
          ),
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
