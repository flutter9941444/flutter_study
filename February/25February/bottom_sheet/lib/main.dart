import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DemoBottomSheet(),
    );
  }
}

class DemoBottomSheet extends StatefulWidget {
  const DemoBottomSheet({super.key});

  @override
  State<DemoBottomSheet> createState() => _DemoBSState();
}

class _DemoBSState extends State<DemoBottomSheet> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _addController = TextEditingController();
  final TextEditingController _collegeController = TextEditingController();

  String name = "";
  String add = "";
  String college = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bottom Sheet Assignement"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            const Text("Create Bottom Sheet"),
            (name == "")
                ? const SizedBox()
                : Container(
                    height: 500,
                    width: 500,
                    color: Colors.greenAccent,
                    child: Column(
                      children: [
                        Text("Name : $name"),
                        Text("Add : $add"),
                        Text("College : $college")
                      ],
                    ),
                  )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: ((context) {
                return Column(
                  children: [
                    const Text("Enter Name "),
                    TextField(
                      controller: _nameController,
                      decoration: const InputDecoration(
                        hintText: "Enter Name",
                      ),
                    ),
                    const Text("Enter College "),
                    TextField(
                      controller: _collegeController,
                      decoration: const InputDecoration(
                        hintText: "Enter College",
                      ),
                    ),
                    const Text("Enter Address "),
                    TextField(
                      controller: _addController,
                      decoration: const InputDecoration(
                        hintText: "Enter Address",
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          name = _nameController.text;
                          college = _collegeController.text;
                          add = _addController.text;
                          setState(() {});
                          Navigator.pop(context);
                        },
                        child: const Text("Submit")),
                  ],
                );
              }));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
