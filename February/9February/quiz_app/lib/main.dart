import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State {
  List<Map> allQuestions = [
    {
      "question": "Who is Founder of Microsoft ? ",
      "options": ["Steve Jobs", "Jeff Bezoz", "Bill Gates", "Elon Musk"],
      "correct": 2
    },
    {
      "question": "Who is Founder of Apple ?",
      "options": ["Steve Jobs", "Jeff Bezoz", "Bill Gates", "Elon Musk"],
      "correct": 0
    },
    {
      "question": "Who is Founder of Amazon ?",
      "options": ["Steve Jobs", "Jeff Bezoz", "Bill Gates", "Elon Musk"],
      "correct": 1
    },
    {
      "question": "Who is Founder of Tesla ?",
      "options": ["Steve Jobs", "Jeff Bezoz", "Bill Gates", "Elon Musk"],
      "correct": 3
    },
    {
      "question": "Who is Founder of Google ?",
      "options": ["Steve Jobs", "Lary Page", "Bill Gates", "Elon Musk"],
      "correct": 1
    },
  ];

  Color colorA = Colors.deepPurple;
  Color colorB = Colors.deepPurple;
  Color colorC = Colors.deepPurple;
  Color colorD = Colors.deepPurple;
  int result = 0;
  bool selectedOnce = false;
  int selected = -1;
  int queNo = 0;
  bool questionScreen = true;
  String msg = "";

  void giveCorrect() {
    int correctOpt = allQuestions[queNo]["correct"];
    if (correctOpt == 0) {
      colorA = Colors.green;
    } else if (correctOpt == 1) {
      colorB = Colors.green;
    } else if (correctOpt == 2) {
      colorC = Colors.green;
    } else {
      colorD = Colors.green;
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple,
          title: const Text("Quiz App",
              style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.italic)),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.deepPurple,
            onPressed: () {
              setState(() {
                if (selectedOnce == false) {
                  msg = "Please select an option";
                } else {
                  msg = "";
                  selectedOnce = false;
                  if (queNo == allQuestions.length - 1) {
                    questionScreen = false;
                  } else {
                    queNo++;
                  }
                  colorA = Colors.deepPurple;
                  colorB = Colors.deepPurple;
                  colorC = Colors.deepPurple;
                  colorD = Colors.deepPurple;
                }
              });
            },
            child: const Icon(Icons.forward)),
        body: Column(
          children: [
            const SizedBox(
              height: 60,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Questions : ${queNo + 1} / ${allQuestions.length}",
                    style: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                    ))
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
                width: 300,
                child: Text(allQuestions[queNo]["question"],
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w400,
                    ))),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                onPressed: () {
                  selected = 0;
                  setState(() {
                    if (!selectedOnce) {
                      if (selected != allQuestions[queNo]["correct"]) {
                        colorA = Colors.red;
                        giveCorrect();
                      } else {
                        result++;
                        colorB = Colors.green;
                      }
                      selectedOnce = true;
                    }
                  });
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(colorA),
                ),
                child: Text("A. ${allQuestions[queNo]["options"][0]}",
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.w500)),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                onPressed: () {
                  selected = 1;
                  setState(() {
                    if (!selectedOnce) {
                      if (selected != allQuestions[queNo]["correct"]) {
                        colorB = Colors.red;
                        giveCorrect();
                      } else {
                        result++;
                        colorB = Colors.green;
                      }
                      selectedOnce = true;
                    }
                  });
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(colorB)),
                child: Text("B. ${allQuestions[queNo]["options"][1]}",
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.w500)),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                onPressed: () {
                  selected = 2;
                  setState(() {
                    if (!selectedOnce) {
                      if (selected != allQuestions[queNo]["correct"]) {
                        colorC = Colors.red;
                        giveCorrect();
                      } else {
                        result++;
                        colorC = Colors.green;
                      }
                      selectedOnce = true;
                    }
                  });
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(colorC)),
                child: Text("C. ${allQuestions[queNo]["options"][2]}",
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.w500)),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                onPressed: () {
                  selected = 3;
                  setState(() {
                    if (!selectedOnce) {
                      if (selected != allQuestions[queNo]["correct"]) {
                        colorD = Colors.red;
                        giveCorrect();
                      } else {
                        result++;
                        colorD = Colors.green;
                      }
                      selectedOnce = true;
                    }
                  });
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(colorD),
                ),
                child: Text("D. ${allQuestions[queNo]["options"][3]}",
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.w500)),
              ),
            ),
            const SizedBox(height: 10),
            Text(msg,
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.normal)),
          ],
        ),
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.deepPurple,
            title: const Text("Quiz App",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic)),
            centerTitle: true,
          ),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (result != 0)
                      ? SizedBox(
                          width: 300,
                          child: Text(
                              "Congratulations You have Scored $result out of ${allQuestions.length}",
                              style: const TextStyle(
                                  fontSize: 20, fontStyle: FontStyle.italic)))
                      : SizedBox(
                          width: 300,
                          child: Text(
                              "Upps.. You Have Scored $result out of ${allQuestions.length} Better Luck Next Time..",
                              style: const TextStyle(
                                  fontSize: 20, fontStyle: FontStyle.italic)),
                        ),
                  const SizedBox(height: 30),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          questionScreen = true;
                          queNo = 0;
                          result = 0;
                        });
                      },
                      style: const ButtonStyle(
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.deepPurple)),
                      child: const Text("Reset"))
                ],
              )
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
