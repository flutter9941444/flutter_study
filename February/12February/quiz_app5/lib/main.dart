import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false, home: QuizApp());
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizApp();
}

class QuestionModel {
  final String? question;
  final List<String>? options;
  final int? correct;
  const QuestionModel({this.question, this.options, this.correct});
}

class _QuizApp extends State {
  //variables
  bool isHome = true;
  bool questionScreen = false;
  int queNo = 0;
  int selected = -1;
  int result = 0;
  String msg = "";

  //questionList
  List allQuestions = [
    const QuestionModel(
        question: "What is Flutter?",
        options: [
          "Flutter is an open-source backend development framework",
          "Flutter is an open-source UI toolkit",
          "Flutter is an open-source programming language for cross-platform applications",
          "Flutters is a DBMS toolkit"
        ],
        correct: 1),
    const QuestionModel(
        question:
            "Who developed the Flutter Framework and continues to maintain it today?",
        options: ["Facebook", "Microsoft", "Google", "Oracle"],
        correct: 2),
    const QuestionModel(
      question:
          "Which programming language is used to build Flutter applications?",
      options: ["Kotlin", "Java", "Go", "Dart"],
      correct: 3,
    ),
    const QuestionModel(
        question: "How many types of widgets are there in Flutter?",
        options: ["2", "4", "6", "8+"],
        correct: 0),
    const QuestionModel(
        question:
            "Which function will return the widgets attached to the screen as a root of the widget tree to be rendered on screen?",
        options: ["main()", "runApp()", "container()", "root()"],
        correct: 1)
  ];
  //methods
  MaterialStateProperty<Color?> giveColor(int index) {
    if (selected != -1) {
      if (index == allQuestions[queNo].correct) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (selected == index) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(Colors.deepPurple);
      }
    } else {
      return const MaterialStatePropertyAll(Colors.deepPurple);
    }
  }

  SizedBox displayResult() {
    if (result == 0) {
      return SizedBox(
        width: 300,
        child: Column(children: [
          const Text("Upppss...."),
          const SizedBox(height: 20),
          Text("You have Scored $result out of ${allQuestions.length}")
        ]),
      );
    } else {
      return SizedBox(
        width: 300,
        child: Column(children: [
          Image.asset("assets/TROPHY.jpeg"),
          const Text("Congratulations !!!"),
          const SizedBox(height: 20),
          Text("You have Scored $result out of ${allQuestions.length}")
        ]),
      );
    }
  }

  void checkPageValidation() {
    if (selected == -1) {
      msg = "Please select an option";
    } else {
      if (queNo == allQuestions.length - 1) {
        questionScreen = false;
      } else {
        if (selected == allQuestions[queNo].correct) {
          result++;
        }
        queNo++;
        selected = -1;
      }
    }
    setState(() {});
  }

  Scaffold screen() {
    if (isHome) {
      return Scaffold(
          appBar: AppBar(
            title: const Text("Quiz App",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500)),
            backgroundColor: Colors.deepPurple,
          ),
          body: Column(
            children: [
              const Row(),
              const SizedBox(
                height: 50,
              ),
              Image.asset("assets/home.jpeg"),
              const SizedBox(height: 20),
              const Text("Welcome to the Quiz App....",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600)),
              const SizedBox(
                height: 30,
              ),
              const Text("Quiz Topic : Computer Engineering",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
              const SizedBox(height: 10),
              Text(("Total Questions : ${allQuestions.length}"),
                  style: const TextStyle(
                      fontSize: 25, fontWeight: FontWeight.w600)),
              const SizedBox(height: 20),
              SizedBox(
                  width: 250,
                  child: ElevatedButton(
                    onPressed: () {
                      isHome = false;
                      questionScreen = true;
                      setState(() {});
                    },
                    style: const ButtonStyle(
                        backgroundColor:
                            MaterialStatePropertyAll(Colors.deepPurple)),
                    child: const Text("Start Quiz"),
                  ))
            ],
          ));
    } else {
      if (questionScreen) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.deepPurple,
              title: const Text(
                "Quiz App",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
              ),
            ),
            floatingActionButton: FloatingActionButton(
                backgroundColor: Colors.deepPurple,
                onPressed: checkPageValidation,
                child: const Icon(Icons.forward)),
            body: Column(
              children: [
                const SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Questions : ${queNo + 1} / ${allQuestions.length}",
                        style: const TextStyle(
                            fontSize: 30, fontWeight: FontWeight.w500)),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                    width: 300,
                    child: Text("${allQuestions[queNo].question}",
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400))),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                    width: 300,
                    child: ElevatedButton(
                        style: ButtonStyle(backgroundColor: giveColor(0)),
                        onPressed: () {
                          if (selected == -1) {
                            selected = 0;
                            setState(() {});
                          }
                        },
                        child: Text("${allQuestions[queNo].options[0]}",
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500)))),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                    width: 300,
                    child: ElevatedButton(
                        style: ButtonStyle(backgroundColor: giveColor(1)),
                        onPressed: () {
                          if (selected == -1) {
                            selected = 1;
                            setState(() {});
                          }
                        },
                        child: Text("${allQuestions[queNo].options[1]}",
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500)))),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                    width: 300,
                    child: ElevatedButton(
                        style: ButtonStyle(backgroundColor: giveColor(2)),
                        onPressed: () {
                          if (selected == -1) {
                            selected = 2;
                            setState(() {});
                          }
                        },
                        child: Text("${allQuestions[queNo].options[2]}",
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500)))),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                    width: 300,
                    child: ElevatedButton(
                        style: ButtonStyle(backgroundColor: giveColor(3)),
                        onPressed: () {
                          if (selected == -1) {
                            selected = 3;
                            setState(() {});
                          }
                        },
                        child: Text("${allQuestions[queNo].options[3]}",
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500)))),
                const SizedBox(
                  height: 10,
                ),
              ],
            ));
      } else {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.deepPurple,
              title: const Text(
                "Quiz App",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
              ),
            ),
            body: Column(
              children: [
                const Row(),
                const SizedBox(height: 20),
                displayResult(),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                    width: 200,
                    child: ElevatedButton(
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.deepPurple)),
                        onPressed: () {
                          queNo = 0;
                          questionScreen = true;
                          result = 0;
                        },
                        child: const Text("Reset"))),
                SizedBox(
                    width: 200,
                    child: ElevatedButton(
                        style: const ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.deepPurple)),
                        onPressed: () {
                          queNo = 0;
                          questionScreen = false;
                          isHome = true;
                          result = 0;
                        },
                        child: const Text("Back To Home")))
              ],
            ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return screen();
  }
}
