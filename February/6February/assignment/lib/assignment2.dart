import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  const Counter({super.key});

  @override
  State createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  List<int> numberLst = [];
  int _count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("ListView builder"),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _count++;
              numberLst.add(_count);
            });
          },
          child: const Text("Next"),
        ),
        body: ListView.builder(
          itemCount: numberLst.length,
          itemBuilder: ((context, index) {
            return Center(
                child: Container(
              height: 30,
              width: 50,
              color: const Color.fromARGB(255, 220, 135, 135),
              margin: const EdgeInsets.all(10),
              child: Text('${numberLst[index]}',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  )),
            ));
          }),
        ));
  }
}
