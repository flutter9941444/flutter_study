import 'package:flutter/material.dart';

class DisplayImage extends StatefulWidget {
  const DisplayImage({super.key});

  @override
  State createState() => _DisplayImageState();
}

class _DisplayImageState extends State<DisplayImage> {
  List<String> imageList = [
    "https://images.unsplash.com/photo-1607992922515-7e38329e65d4?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8bmF0dXJlJTIwaW1hZ2VzfGVufDB8fDB8fHww",
    "https://images.unsplash.com/photo-1626594995085-36b551227b9a?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8bmF0dXJlJTIwaW1hZ2VzfGVufDB8fDB8fHww",
    "https://images.unsplash.com/photo-1544961371-516024f8e267?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTB8fG5hdHVyZSUyMGltYWdlc3xlbnwwfHwwfHx8MA%3D%3D",
    "https://images.unsplash.com/photo-1616352116682-52d7941afd53?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fG5hdHVyZSUyMGltYWdlc3xlbnwwfHwwfHx8MA%3D%3D0000000000000",
    "https://images.unsplash.com/photo-1613706903647-77e255eff994?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTR8fG5hdHVyZSUyMGltYWdlc3xlbnwwfHwwfHx8MA%3D%3D",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("ListView builder"),
          centerTitle: true,
        ),
        body: ListView.separated(
          itemCount: imageList.length,
          itemBuilder: ((context, index) {
            return Container(
              height: 300,
              width: 100,
              //color: Colors.black,
              margin: const EdgeInsets.all(2),
              child: Image.network(
                imageList[index],
                height: 300,
                width: 100,
              ),
            );
          }),
          separatorBuilder: (context, index) {
            return const SizedBox(height: 5, width: 5);
          },
        ));
  }
}
