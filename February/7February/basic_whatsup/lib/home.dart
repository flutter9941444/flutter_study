import 'package:flutter/material.dart';

class WhatsUp extends StatefulWidget {
  const WhatsUp({super.key});

  @override
  State createState() => _WhatsUpState();
}

class _WhatsUpState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text(
              "WhatsApp",
              style: TextStyle(fontWeight: FontWeight.w400),
            ),
            backgroundColor: const Color.fromARGB(255, 7, 94, 84),
            actions: [
              IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.add_a_photo_outlined)),
              IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
              IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert)),
            ]),
        floatingActionButton: FloatingActionButton(
          backgroundColor: const Color.fromARGB(255, 7, 94, 84),
          onPressed: () {},
          child: const Icon(
            Icons.message,
            color: Colors.white,
          ),
        ),
        body: Column(children: [
          Container(
              height: 30,
              color: const Color.fromARGB(255, 7, 94, 84),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.people, color: Colors.white),
                  Text("Chats", style: TextStyle(color: Colors.white)),
                  Text("Updates", style: TextStyle(color: Colors.white)),
                  Text("Calls", style: TextStyle(color: Colors.white)),
                ],
              )),
          Expanded(
            child: ListView(shrinkWrap: true, children: [
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 1"),
              ]),
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 2"),
              ]),
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 3"),
              ]),
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 4"),
              ]),
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 5"),
              ]),
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 6"),
              ]),
              const SizedBox(
                height: 5,
              ),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 7"),
              ]),
              const SizedBox(height: 5),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 8"),
              ]),
              const SizedBox(height: 5),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 9"),
              ]),
              const SizedBox(height: 5),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 10"),
              ]),
              const SizedBox(height: 5),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 11"),
              ]),
              const SizedBox(height: 5),
              Row(children: [
                const SizedBox(width: 5),
                Image.network(
                    "https://cdn-icons-png.flaticon.com/128/2920/2920045.png",
                    height: 50,
                    width: 50),
                const SizedBox(width: 10),
                const Text("User 12"),
              ]),
              const SizedBox(height: 5),
            ]),
          )
        ]));
  }
}
