import 'package:flutter/material.dart';

class Assign2 extends StatelessWidget {
  const Assign2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment2"),
        ),
        body: Center(
          child: Container(
              height: 100,
              width: 100,
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              color: Colors.green,
              child: Container(
                color: Colors.deepPurple,
              )),
        ));
  }
}
