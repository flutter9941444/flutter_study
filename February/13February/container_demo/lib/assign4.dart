import 'package:flutter/material.dart';

class Assign4 extends StatelessWidget {
  const Assign4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment4"),
        ),
        body: Center(
          child: Container(
              height: 300,
              width: 300,
              color: Colors.green,
              child: Container(
                margin: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
                color: Colors.deepPurple,
              )),
        ));
  }
}
