import 'package:flutter/material.dart';

class Assign6 extends StatelessWidget {
  const Assign6({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment6"),
        ),
        body: Center(
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: Colors.blue,
                  width: 5,
                )),
          ),
        ));
  }
}
