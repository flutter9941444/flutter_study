import 'package:flutter/material.dart';

class Assign3 extends StatelessWidget {
  const Assign3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment3"),
        ),
        body: Center(
          child: Container(
              height: 300,
              width: 300,
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 10, bottom: 10),
              color: Colors.green,
              child: Container(
                color: Colors.deepPurple,
              )),
        ));
  }
}
