import 'package:flutter/material.dart';

class Assign1 extends StatelessWidget {
  const Assign1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment1"),
        ),
        body: Center(
          child: Container(
            height: 200,
            width: 200,
            color: Colors.red,
          ),
        ));
  }
}
