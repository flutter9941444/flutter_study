import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class TodoModelClass {
  String title;
  String description;
  String date;
  TodoModelClass(
      {required this.title, required this.description, required this.date});
}

class _MyHomePageState extends State<MyHomePage> {
  //Color defaultColor = const Color.fromARGB(255, 47, 179, 139);
  Color defaultColor = const Color.fromARGB(255, 47, 179, 139);
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descripController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  var listOfColors = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(250, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1)
  ];
  bool isEdit = false;
  String msg = "";
  bool isValidate = false;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  //GlobalKey<FormFieldState> usernameKey = GlobalKey<FormFieldState>();
  //GlobalKey<FormFieldState> passwordKey = GlobalKey<FormFieldState>();

  //controllers
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  List<TodoModelClass> cardList = [];
  List validation = [
    {'name': 'namrata', 'pwd': 'Namrata@123'},
    {'name': 'chaudhari', 'pwd': 'namu'}
  ];

  void createSheet(TodoModelClass obj) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        )),
        context: context,
        builder: ((context) {
          return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                const SizedBox(height: 20),
                Text(
                  "Create Task",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w600,
                    fontSize: 25,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Title : ",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(height: 4),
                        TextField(
                            controller: _titleController,
                            decoration: InputDecoration(
                              hintText: "  Enter Title",
                              border: InputBorder.none,
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(color: defaultColor)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: defaultColor,
                                ),
                              ),
                            ),
                            cursorColor: defaultColor),
                        const SizedBox(height: 10),
                        Text(
                          "Description : ",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(height: 4),
                        TextField(
                            controller: _descripController,
                            decoration: InputDecoration(
                              hintText: "  Enter Description",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(color: defaultColor),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: defaultColor,
                                ),
                              ),
                            ),
                            cursorColor: defaultColor),
                        const SizedBox(height: 10),
                        Text(
                          "Date: ",
                          style: GoogleFonts.quicksand(
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(height: 4),
                        TextField(
                            onTap: () async {
                              DateTime? pickedDate = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2024),
                                lastDate: DateTime(2025),
                              );
                              String formatDate =
                                  DateFormat.yMMMd().format(pickedDate!);
                              setState(() {
                                _dateController.text = formatDate;
                              });
                            },
                            controller: _dateController,
                            decoration: InputDecoration(
                              suffixIcon: Icon(
                                Icons.calendar_month_outlined,
                                color: defaultColor,
                              ),
                              hintText: "  Select Date",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(color: defaultColor),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: defaultColor,
                                ),
                              ),
                            ),
                            cursorColor: defaultColor),
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        defaultColor)),
                                onPressed: () {
                                  if (_titleController.text == "" ||
                                      _descripController.text == "" ||
                                      _dateController.text == "") {
                                    setState(() {});
                                  } else {
                                    setState(() {
                                      obj.title = _titleController.text;
                                      obj.description = _descripController.text;
                                      obj.date = _dateController.text;
                                      if (!isEdit) {
                                        cardList.add(obj);
                                      }
                                    });
                                  }
                                  Navigator.pop(context);
                                },
                                child: Text(
                                  "Save",
                                  style: GoogleFonts.quicksand(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 25,
                                      color: Colors.white),
                                )),
                          ],
                        ),
                        const SizedBox(height: 10),
                        Text(msg, style: const TextStyle(color: Colors.red))
                      ]),
                )
              ]));
        }));
  }

  Scaffold isLoggedIn() {
    if (isValidate) {
      return Scaffold(
          appBar: AppBar(
            title: Text(
              "To-Do List",
              style: GoogleFonts.quicksand(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 30,
              ),
            ),
            actions: [
              IconButton(
                  onPressed: () {
                    setState(() {
                      isValidate = false;
                    });
                  },
                  icon: const Icon(Icons.logout_rounded,
                      color: Colors.white, size: 30)),
              const SizedBox(width: 8),
            ],
            centerTitle: true,
            //backgroundColor: defaultColor,
            backgroundColor: defaultColor,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              setState(() {
                _titleController.text = "";
                _descripController.text = "";
                _dateController.text = "";
              });
              isEdit = false;
              createSheet(TodoModelClass(title: "", description: "", date: ""));
            },
            backgroundColor: defaultColor,
            child: const Icon(Icons.add),
          ),
          body: ListView.builder(
            itemCount: cardList.length,
            itemBuilder: (context, index) {
              return Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(10),
                        width: 370,
                        decoration: BoxDecoration(
                          color: listOfColors[index % listOfColors.length],
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 5,
                              spreadRadius: 2.5,
                              offset: Offset(0.2, 0.2),
                            )
                          ],
                          //color: listOfColors[index % listOfColors.length]),
                        ),
                        child: Column(
                          children: [
                            Row(children: [
                              Image.asset(
                                "assets/documents.png",
                                height: 80,
                                width: 80,
                              ),
                              const SizedBox(width: 15),
                              Column(children: [
                                Row(children: [
                                  SizedBox(
                                      width: 253,
                                      child: Text(
                                        cardList[index].title,
                                        style: GoogleFonts.quicksand(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 25,
                                        ),
                                      ))
                                ]),
                                const SizedBox(height: 10),
                                Row(children: [
                                  SizedBox(
                                      width: 253,
                                      child: Text(
                                        cardList[index].description,
                                        style: GoogleFonts.quicksand(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 25,
                                        ),
                                      ))
                                ])
                              ]),
                            ]),
                            const SizedBox(height: 10),
                            Row(children: [
                              Text(cardList[index].date),
                              const Spacer(),
                              Row(children: [
                                GestureDetector(
                                  onTap: () {
                                    _titleController.text =
                                        cardList[index].title;
                                    _descripController.text =
                                        cardList[index].description;
                                    _dateController.text = cardList[index].date;
                                    isEdit = true;
                                    createSheet(cardList[index]);
                                  },
                                  child: Icon(
                                    color: defaultColor,
                                    Icons.edit,
                                    size: 30,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      cardList.remove(cardList[index]);
                                    });
                                  },
                                  child: Icon(
                                    color: defaultColor,
                                    Icons.delete,
                                    size: 30,
                                  ),
                                )
                              ])
                            ])
                          ],
                        ),
                      )
                    ],
                  ));
            },
          ));
    } else {
      return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Padding(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
                top: 70,
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Image.asset(
                        "assets/loginimg.jpg",
                        height: 380,
                        width: 320,
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: username,
                        //key: usernameKey,
                        decoration: InputDecoration(
                          hintText: "Enter username",
                          label: Text("Username",
                              style: TextStyle(color: defaultColor)),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide(width: 2),
                            //borderRadius: BorderRadius.circular(20),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide:
                                BorderSide(color: defaultColor, width: 2),
                          ),
                          prefixIcon: Icon(Icons.person, color: defaultColor),
                        ),
                        validator: (value) {
                          print("I am in username validatior");
                          if (value == null || value.isEmpty) {
                            return "Please enter username";
                          } else {
                            for (int i = 0; i < validation.length; i++) {
                              print(validation[i]['name']);
                              if (validation[i]['name'] == username.text) {
                                return null;
                              }
                            }
                            return "Enter valid username";
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          controller: password,
                          //key: passwordKey,
                          obscureText: true,
                          obscuringCharacter: "*",
                          decoration: InputDecoration(
                              hintText: "Enter password",
                              label: Text("Password",
                                  style: TextStyle(color: defaultColor)),
                              border: const OutlineInputBorder(
                                borderSide: BorderSide(width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: defaultColor, width: 2),
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(
                                Icons.lock,
                                color: defaultColor,
                              ),
                              suffixIcon: Icon(Icons.remove_red_eye_outlined,
                                  color: defaultColor)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Please enter password";
                            } else {
                              for (int i = 0; i < validation.length; i++) {
                                if (validation[i]['name'] == username.text) {
                                  if (validation[i]['pwd'] == password.text) {
                                    return null;
                                  } else {
                                    return "Enter valid  password";
                                  }
                                }
                              }
                              return "Enter valid password";
                            }
                          }),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        style: ButtonStyle(
                            fixedSize:
                                const MaterialStatePropertyAll(Size(150, 50)),
                            backgroundColor:
                                MaterialStatePropertyAll(defaultColor)),
                        onPressed: () {
                          //bool loginValidate1 =
                          //  usernameKey.currentState!.validate();
                          // bool loginValidate2 =
                          //   passwordKey.currentState!.validate();
                          bool formValidate = formKey.currentState!.validate();
                          print(formValidate);

                          if (formValidate) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              backgroundColor: defaultColor,
                              content: const Text("Login Successful"),
                            ));
                            setState(() {
                              isValidate = true;
                            });
                          } else {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              backgroundColor: Colors.red,
                              content: Text("Invalid password or username"),
                            ));
                          }
                        },
                        child: const Text("Login",
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                      )
                    ],
                  ))),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isLoggedIn();
  }
}
