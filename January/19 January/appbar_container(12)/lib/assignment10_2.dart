//create an appbar, give an Icon at the end of the appbar and give title in middle of the appbar
import 'package:flutter/material.dart';

class Assignment10_2 extends StatelessWidget {
  const Assignment10_2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      actions: const [
        Icon(
          Icons.mail_outline,
        ),
      ],
      title: const Text(
        "E-Mail",
      ),
      centerTitle: true,
    ));
  }
}
