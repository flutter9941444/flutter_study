//create a screen with deep purple color app bar titled "Hello Core2web" and in
// the title in the center of body create blue container with(width:360, height:200)
import 'package:flutter/material.dart';

// ignore: camel_case_types
class Assignment10_3 extends StatelessWidget {
  const Assignment10_3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Hello Core2web",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: Colors.deepPurple,
        ),
        body: Center(
          child: Container(
            height: 200,
            width: 360,
            color: Colors.blue,
          ),
        ));
  }
}
