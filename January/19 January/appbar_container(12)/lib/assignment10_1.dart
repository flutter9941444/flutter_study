//create an appbar add a title and add any two icons at the end of the appbar and give color to the appbar
import 'package:flutter/material.dart';

// ignore: camel_case_types
class Assignment10_1 extends StatelessWidget {
  const Assignment10_1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: const [
          Icon(
            Icons.favorite_outline_rounded,
            color: Colors.white,
          ),
          SizedBox(
            width: 5,
          ),
          Icon(
            Icons.message_rounded,
            color: Colors.white,
          ),
          SizedBox(
            width: 5,
          ),
        ],
        title: const Text(
          "Instagram",
          style: TextStyle(
            fontStyle: FontStyle.italic,
          ),
        ),
        backgroundColor: Colors.black,
      ),
    );
  }
}
