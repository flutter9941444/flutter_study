//In the screen, show 3 assests images of size(width:150,height:150)
import 'package:flutter/material.dart';

class Assignment10_5 extends StatelessWidget {
  const Assignment10_5({super.key});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Images")),
      body: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/img1.jpg",
            width: 100,
            height: 100,
          ),
          const SizedBox(
            width: 20,
          ),
          Image.asset("assets/img2.jpg", width: 100, height: 100),
          const SizedBox(
            width: 20,
          ),
          Image.asset("assets/img3.jpg", width: 100, height: 100),
        ],
      )),
    );
  }
}
