//create a screen which add 5 network images horizontally with size(width:150,height:300) and make scrollable
import 'package:flutter/material.dart';

class Assignment10_7 extends StatelessWidget {
  const Assignment10_7({super.key});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Container Scroll")),
      body: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Image.network(
                  "https://cdn.pixabay.com/photo/2017/02/01/22/02/mountain-landscape-2031539_640.jpg"),
              const SizedBox(
                width: 10,
              ),
              Image.network(
                  "https://cdn.pixabay.com/photo/2016/08/11/23/48/mountains-1587287_640.jpg"),
              const SizedBox(
                width: 10,
              ),
              Image.network(
                  "https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_640.jpg"),
              const SizedBox(
                width: 10,
              ),
              Image.network(
                  "https://cdn.pixabay.com/photo/2018/01/12/10/19/fantasy-3077928_640.jpg"),
              const SizedBox(
                width: 10,
              ),
              Image.network(
                  "https://cdn.pixabay.com/photo/2016/11/14/04/36/boy-1822614_640.jpg"),
            ],
          )),
    );
  }
}
