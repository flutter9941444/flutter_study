//create 2 containers in the center of the screen and give that color to the containers
import 'package:flutter/material.dart';

// ignore: camel_case_types
class Assignment10_4 extends StatelessWidget {
  const Assignment10_4({super.key});
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment10_4",
              style: TextStyle(
                color: Colors.black,
              )),
        ),
        body: Center(
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(height: 100, width: 200, color: Colors.lightBlue),
            const SizedBox(
              width: 20,
            ),
            Container(height: 100, width: 200, color: Colors.lightGreen),
          ]),
        ));
  }
}
