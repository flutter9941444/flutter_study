//Create a screen in which add 10 colorful containers vertically and make scrren scrollable
import 'package:flutter/material.dart';

class Assignment10_6 extends StatelessWidget {
  const Assignment10_6({super.key});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Container Scroll")),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            height: 250,
            width: 250,
            color: Colors.amber,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.pink,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.lightGreen,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.lightBlue,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.deepPurple,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.red,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.indigo,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.lime,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.brown,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 250,
            width: 250,
            color: Colors.cyan,
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      )),
    );
  }
}
