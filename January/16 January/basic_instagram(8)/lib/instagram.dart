import 'package:flutter/material.dart';

class Instagram extends StatefulWidget {
  const Instagram({super.key});
  State<Instagram> createState() => _Instagram();
}

class _Instagram extends State<Instagram> {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            "Instagram",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.black,
              fontSize: 30,
            ),
          ),
          actions: [
            const Icon(
              Icons.favorite_rounded,
              color: Colors.red,
            )
          ],
        ),
        body: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(
                  "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?cs=srgb6d1=pexels-james-wheeler-414612.jpg&fm=jpg",
                  width: double.infinity,
                ),
                Row(
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.favorite_outline_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.favorite_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.send,
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ));
  }
}
