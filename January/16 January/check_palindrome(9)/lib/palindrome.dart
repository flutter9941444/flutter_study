import 'package:flutter/material.dart';

class Palindrome extends StatefulWidget {
  const Palindrome({super.key});
  State<Palindrome> createState() => _Palindrome();
}

class _Palindrome extends State<Palindrome> {
  int? count1 = 0;
  int? count2 = 0;
  int? count3 = 0;

  final List<int> noLst = [10, 121, 153, 145, 370, 1221];
  void checkPalindrome() {
    print("I am in Palindrome function");
    int countNo = 0;
    for (int i = 0; i < noLst.length; i++) {
      int no = noLst[i];
      int rev = 0;
      while (no != 0) {
        rev = rev * 10 + no % 10;
        no = no ~/ 10;
      }
      if (rev == noLst[i]) {
        countNo++;
        print("Palindrome");
      } else {
        print("object");
      }
    }
    setState(() {
      count1 = countNo;
    });
  }

  void checkAmstrong() {
    int countNo = 0;
    for (int i = 0; i < noLst.length; i++) {
      int no = noLst[i];
      int count = 0;
      while (no != 0) {
        count++;
        no = no ~/ 10;
      }
      no = noLst[i];
      int num = 0;
      while (no != 0) {
        int mul = 1;
        int temp = no % 10;
        for (int i = 0; i < count; i++) {
          mul = mul * temp;
        }
        num = num + mul;
        no = no ~/ 10;
      }
      if (num == noLst[i]) {
        countNo++;
      }
    }
    setState(() {
      count2 = countNo;
    });
  }

  int calcFactorial(int num) {
    int fact = 1;
    for (int i = num; i > 1; i--) {
      fact *= i;
    }
    return fact;
  }

  void checkStrong() {
    int countNo = 0;
    for (int i = 0; i < noLst.length; i++) {
      int no = noLst[i];
      int num = 0;
      while (no != 0) {
        num += calcFactorial(no % 10);
        no = no ~/ 10;
      }
      if (noLst[i] == num) {
        countNo++;
      }
    }
    setState(() {
      count3 = countNo;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Palindrome Amstrong Strong Numbers",
        ),
        backgroundColor: Colors.lightGreen,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("$noLst"),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: checkPalindrome,
                  child: const Text("Check Palindrome"),
                ),
                const SizedBox(
                  width: 20,
                ),
                Text(
                  "$count1",
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: checkAmstrong,
                  child: const Text("Check Amstrong"),
                ),
                const SizedBox(
                  width: 20,
                ),
                Text(
                  "$count2",
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: checkStrong,
                  child: const Text("Check Strong"),
                ),
                const SizedBox(
                  width: 20,
                ),
                Text(
                  "$count3",
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      count1 = 0;
                      count2 = 0;
                      count3 = 0;
                    });
                  },
                  child: const Text("Reset"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
