import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {
  //variable
  int? selectedIndex = 0;
  //list of images
  final List<String> imageList = [
    "https://cdn.pixabay.com/photo/2023/12/08/15/30/bird-8437764_1280.jpg",
    "https://media.istockphoto.com/id/1151755587/photo/sunrise-behind-a-tropical-island-in-the-maldives.jpg?s=1024x1024&w=is&k=20&c=rFzBaIblaDbkFSVyQOjXCcCmDLTWgTCqmJWqMLVcZQ4="
  ];
  void showNextImage() {
    setState(() {
      if (selectedIndex! < imageList.length - 1) {
        selectedIndex = selectedIndex! + 1;
        print("I am here1");
      } else {
        selectedIndex = 0;
        print("I am here2");
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Display Images",
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              imageList[selectedIndex!],
              width: 200,
              height: 200,
            ),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: showNextImage,
              child: const Text(
                "Next",
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  selectedIndex = 0;
                });
              },
              child: const Text(
                "Reset",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
