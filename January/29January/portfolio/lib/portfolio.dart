// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class portfolio extends StatefulWidget {
  const portfolio({super.key});
  State<portfolio> createState() => _portfolio();
}

class _portfolio extends State<portfolio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Portfolio"),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Text("Next"),
        onPressed: () {},
      ),
      body: Container(
        width: 400,
        height: 400,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 2.0,
          ),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: 396,
                  height: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      width: 2.0,
                    ),
                  ),
                  child: const Text("Portfolio"),
                ),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            const Row(
              children: [
                Text("Name : Namrata Chaudhari"),
              ],
            ),
            Row(
              children: [
                const SizedBox(
                  width: 40,
                ),
                Image.asset(
                  "assets/profile.jpg",
                  height: 50,
                  width: 50,
                ),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            const Row(
              children: [
                Text("College Name : JSPM Narhe Technical Campus"),
              ],
            ),
            Row(
              children: [
                const SizedBox(
                  width: 40,
                ),
                Image.network(
                  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK0AAACgCAMAAABJ7xyvAAACdlBMVEX////jHiT+/v4rKin3urzpTFDjISf75ubwjpHzoqb99PT4zc73wsLrZGnnOj/74uPvgYTrYGT0rbD87e3609Ttb3OBgYBlZGSQkI9ubm2uISV1JSdWVVTExMREQ0JeXl1QJyj09PTOzs6cIiY7OjlDKCjZHiSCJCY7KSjIHyS4tra+ICUzKSjn5+fymp1PT06pqanb29ufnp7pUFViJidtJSflLDF7enpaJyeXIyalIiVLKCj+/PQ4KSiMIybSHyTx0JPWQChrTDPvxlbt8/lQMyzh2Mrd09Dv6+Xj5u3MzMK/YFt8JCbueX3pTVLoQUb45JH89uLRpoTPnDTOl1fbjXrLbijPZC7hwKDTj0jy0Gj41ln01Ybful3QqmDpozXriSnjiUjslRm4iFP01MjIckq2dy6ndy26f2a6moG0SCenSyfTuqj3vUzKii/psEvLiFz0yCfio3PytS7347HGeDXNely1lmrw0qW2gE/RhE+liEflp2vPl02xikrZnHzSqnOYekbxv4t4Ykf44L2Rf0XFkEV5aEqhaDyJUzXWw3zto3iFWEPtxHpiSD2QSSzrZTp2SDF2fF68Y0nlY0tmWDiTZ0Lkh1qUc2SVh3aWkH7YyaOfZCqykSukkW3hcCy5VlK+sE7NhIZqTEHFTGGooVGijlqJNkqCQEHsqGDhe2rSqD3Yx6u/qCyrnSrHoGi/k32TN1DNpDN8ZDHvuAx3XFifd1myhn6wa1znlz2ERTi0spv68cvgoBf74Sv3zEKls8v57YKnroi2xrJ3lG+uyJtukl1AUzRIfzpBZzWBr2OeqK68v2ROXDWltIFlbViEiUbssZdhvbBEAAAXcElEQVR4nNWci18TV77A5QQBAQV5hkeACSAhMJDwVoKEQJJJ1NUA5WVCADWKhgSElEAVEVOK1tC17boKgutad2GlF1co2xgordrd6+29t7f/0T1nJu8XARNkfx8+kMecme/8zu95TsK+fUEREJzTBkeiK6I/NIL/Eh0WEvZvgwthQ/5tcEnYfxdcEjb2Q+JGRvgtiUivoSAU/Un0b0ig7yomZFsSCgCF659EfFBaCHtGtg3cQNNGHvBPYi2wjU3NkRRurB+jogJM66eQDobM4KOW1nDKGPZuZLDBgrb2dh7Y27h2WFlHx1m5bE/j2mFFivqOjvpOYg/j2mF5yq5uKD3nwJ7FtcMCcP6C6uJF1SUR2Ku4jrAA9F5uabmitjz5gLiRniXUCZav7OvrG+xpc8QN9TLSqwSC9pDPDEaJSKNV3OhXKE0iB9ztyuHg0tpgeZ0DZ6+Oftx8drAZ7Bw3ILSlh9ylwgkWDOmWlz8evtE/3KwbcsKt8DDWqyQGgtaDODsYAPqR5dH+Tz75uP/clB444u6FyOAKC4zN1641jzRfM7br5GCP4brBgv7rY/ONN66O32zWTIC9hesOCxY1t2QymWGo86bJkXYP4HqABYufzpuXftPP37w1KQd7CdcTLG/ys+Gp2xMjt1UjN++APYTrCRZ8Lli7O33XqHs6PXX/fvjewfUIK6u7Pa9vbOMb9ROKC5f/CLzjgl1dK/MICwx1X9wwGPT6CYN+caru9+FecMEu03qGhbq9Z55ou9bcPvH5hS/rvnZ734ILgrQMCcI9SpxnWAC+Evzh6sTo1193d0/9Ie8rwjNunOeTWuQ9aCO9Vx6eYGVCYdN9vv6P3dr2sZtC4YxnXJ9SERRaR1gZ30D+XXpYOP3w/sSE3th95cHDHFrhr260vOhg0u53l9MusAaRfqKR1OLDwkLhbZ1e33+lteX2RXZyYSHb8Z6WiHXzzGxjHRx+2sN5KQlIsWgXVwfjTeivmdpRR84R3rr8aG6SP6Q3ak1jc70c1Qrdzko8nG980Ns4Nqk6tntx1y0aGHStrb/vRrqdb3ncOtfZOc7nG7Wd5+Z6GaqeJmsHAT57mD7dOH5i7PbtP/0pf7dw3UMXb6L789ZFqFvD498/bp180n+nc2J5vHPkiVpycaOnZZbS7Hp6zoM/n5gSCJ4KF775W9ju4HqIszz+4uf6IUjbWgdF8eTKsHZsWTsy94wrmasf7OrkwvvQd/5l/tKf/iq895QNCOGr54pdScIek4Lhmp4v44G2fERr6nw+ea353PL1J8MYPnd2UytntPU2X1t89M3CSm4Gh4cRjNz7TR27UTN4SbeLE3IeD/ytDuK2Ppt8cv2cqbN/+YkIiJ6fPaudfXhbo2lfvM5iCemRWFJNTQ3rbtOZXShxvKRbo2miEdJ+XVeQf+V5ff2z+smRm8NPngHwRqFYGRFMT9828hVaVhJ0xBJWVhVLeLPpbPArMi+wopHJ4eUlAOrqjuVpNs+ebfq2vrmx8ds2gM1M3hQIBJ9dWpgwKqjg8DB34e6t3CkNpA0yrhdYQLQr+F1tgKitS/1iY2BTq/0PIy5efq4WMwT3BYK1S99ksUZhSNOLoG6JzGnhtHAOWkJAcKNjvclBb7UBb7TFpJ/gGepqL+s2Bvt+kpuMavFY05oA2sCDS9/8lVUo1PGNppYvFqB6ed/V3HulIFd3LTXDQbcrFflNW+Qzh3uCBeB8+9UJA/9Ma11BS0/PT0OEqa++QwUj6/RnkDW3pviFpsVoNN2uTrsLtWsWCO/yrCM9lzilgaH1DAt4WsWQQa/trLt8uVVhkEiWm67fh3q99eKbv1fX1MyAxg2dSaspezV9bxgeLVbz7EM94vpPmxDnUSJ8wAIg18oN/GeTH1++PDXOZDJfPEKsaycQq2AJ8LqvLWuVui9WVu4JeK5DqZ0f54u9r+d5czCLyJR9crlKZ+5dy1Gz2Q8F0wKKtXBap7gqW9drx1W6kZufrty7h7uNDXxk2AIW8MaNHYOal51Nt2gckvXFib9n1RQKH/WY9AaDYeKa9uq5T8ZWVgSv3KvzgONuCWu4PjLBl/MVTdMZML7eGv7omzJWDeuBarWnj28wDMnb+9cENz/5VFD1wNPwwOJuBQtkMqNu0SBXaC8i1vl+FAhqvhObuwY3jHI+X67V3BOurHw6/SpX5HF8IHG3hF0fMsjb2+WKSZgLBGv97RcFNTXTbLHkXE9Xs7Kvvh7BCu6vCMtYD72cIXC4W8ICpD1T+/P7tyHr8qjqpqCGVSkWi9ktPaOidiUUReM9obBKmMH2eopA4foBK5fLdU1j55ZXhhUq7YhQMP0dW61Wi9cet6glhi6TUq7ni0r+QWdgPk4SGNwtYUVXtVBaNcsTer6qqkywIhBOap8/GxvXbg4qGyWYQjkoH9LrfZzh/XFjoikp2gJW9uL70RaNSXmhm6/n66qqc1mC+7CweTJyf0452NPVLcF4xo162KY1uqUFT7ghRdEOEuMvrWP+9QFr+P77S6oLLV2m0Wt8vulVWS6r5ung4GDf3PD8JDLYq1wcA/w+eZOiedYv7TpJ7A5ofcK+eDEyNXWhpaUFqk+1AGGFYtFi3+DgXJNJOfi8TSLBcYwna1KYFsdl28b1mzY6EcrhLWB5H63NmIebr11tNnbI+YOvoB3UzGAEV6S4btK2DKrMOAYdC8PwG+1aI9/AA5gvP7PiliZaJc5fWpJ4Kweb+PP3d5cIPp+/qDAaN6eqhEKWcIjACLFY2qxpH+gxSyg6TDQqn1jkX2trALCH3Ap3Z662JSyv/8SFj1+YZ6F/8flnny88hbQCbccQgcNg26Ed0EjFOCB5ZXK50WS60t6oZ0hwwPPhcDvFJWEPxXtnBbKpE+2LV81LkJW/+e2CcAHSfrLao5VLpQRfWc+fVIslEnLy5WebtMqulsc63TBXwpP5sODI/TvDJfdDQ/Z7xV1fl/WfGF00Go3QwQa+XXi6IKxisWb52o3VjY25bzc3+7QTbedj4hGuYVWn05lar1x5tCaB1sDzqt1I0lV2sMqYEOYbl7dOLPWTtPUDAxeFCFYonIblq+FM38APm881m5tavX6iLZ6QwNeaW7u1WnWvlAlpgWjIC60FNixh27S+cWXrMhnRIFKZlKurg9C/qqqQIQi/k2IAM5zp2NzcfKkZXNUODRmvigkUxBa79eeZP0okkoYGIPVcib0PrG9c3tA6pJUw1oi1aSGqWNCPkDUjwjACWz+z+cMPg3MtsLrt0CpG1eEEjyCWcDH7RwnWAIVJGDxl4veC3QLXAMEakBfBUABZFx5A2OklMYaLCFzesfrDwNwXLbqLpi7dBbNYjBYSGhjsHxvgmAZ4yFKgYEtd18C94YoISAvjPzH9VP30qRSop6eX1CJCLCIkBv7gwMBcr1rde0731XyvWgztA5OwzdAM8AYgUS95yBIWWNc18y3yWZhrtvaGK4K4GDJJsxhIIQ/8BQiCEGEAF42bVldVs4QYVyMRwwOxBrHaDM0WBw2MXg8ZwgrrKlss63ui9YGLETLcoikCpVkReiIaN3Qor3SIcJQnMHgLULdisVnKxSEs1+zBDrzBbkXrsncV7d12CRKXICxNN9ZgfwciP+rR6hEt9TYmETPMEliRNUjY8+52YLXZaLe9s/jtWLFPV5OSyrWqtsFGS3JP9ijPcCEthoNILFIiEZuZEiYOw8iM1CvsDqOBf7i4lAtn2UKLNVhVRlZe4OUPA2dEuBg9gbckkbClEgkTkzA5M26qDSCsL1yuGNFSmA1WWgxHqsVe/rB6dgnRAoSLi6VcqFcJkz3DDSqsD1wM2gIhsdJaLIF6StK2UbQIlimWNDDZXKmZEWRYH7i4GNGSfA1W5VJ/sZcDqx3jYgOsGTFCJBarYfRisKVsc9BhPeOSfQF0c5ghkFobJJRyUWqFbkbqdrxtCKoWx3F177xEwuVIzWbXUBsEWA+4DRKy54LCIHAE2mDXMTKKBuhl9ePaIQyTidS9vVNmJpNhNs+6htqgwLrhShhMGOpxxCt9CHMUdDVUXkFeirZBAnWrGFeegWawNDu8dkXEZHACDhvvdWfbqWbgsplMLqLDcUL0VRuXC9HJahBxkn8kcwOrWlPPGZiKRUvzX6hEaqhZAtVgmBusx/10fz7SGu8lAbokYcYvbDYDZiasAecSl87jUqmISzApXgkpzLHBDW3P4E8yYql3+P5jzeySetYMh5A3hDnDepTA0JK4kl84DCaDieyWK8alcLqRSLmIl0vRPtvo0WysdgzB19UjPU1tarWZzeQiD2S+8QPWL1oQ40McagaMzWEzIBoMpVwY+AGxvr60vi4ikElwkTBft3apNjb7zrSpe/+iWH3Za+awmaTtvHaGDQv1fLH3/5CQo6vhbDYyXYyQcrmwv5Euzd45cwZ2FOuULM1ebGmBVW69cfnmsmKjSc1miKEd4K/f4kGNBh5xoZMhy0VRS8qVSjHs9dslUdv4+Difkjt3FMqmrpcDAxumsbHxvq4Ns1QqffPzP9++CW7o8ozLYMDJZjCQ00BaGPJFqNSF7r80O6vXv/7yX/+62Kfc0DStrpo6x4yKwQ3z27dvX5txsJuwNtxEFBLIAIth6wQqBVBmiwoNh9F19p+/4v/57g8mrfKl6qKqc3x5qaOnxznS7hKsDffPbAYTuXgDJnsyfIcscrGIUwcTRT//17sZgL179+uXV7Wmly0X2js7+Xzl6nX4Plgf2mVYG+7hX1DMZUL3/++/zMlw0Zs35u+/OvH9u3dfMgHqK4iZn0e1Wk1La7dJId9YVS7FfwTGNaARELsJa7dd5o+/QGIGg/Hmv34mVWb+n89+5dpSFfbr/3Yb+fUbj3t6Bjd6NprWO3jNKlkHdm5XYT2UOISZYuTMOG6M4r/+H1Srsqe1Z3Vg9Xn9TyqNZlU1eUe/u7DuuNiTJ+sOmPFxluan5N2nY6bHF4afPL8z+3q5SaNUNo2JDgYYNvrgVlIa4oRLjGjQd4hwGQDnfzsPwB0ZCP8t+jy+rja//fKzefPM6zdotaZRpzkntqTb0i2v4e+y6IGti4UQJ9w2zUYLAcQ3eBD28/ORAC0h/vb5b2hjwqllfKER+6wNnMTfdXz/aS24+CMV1O0lGRav/gh9zKrrj19D+fi386L2f9hZxeM6leqyx9WV96GNj/JD4hxsV9Q4fiNSfMIm3y3YH9u7xqURjUb3VS36sLA/F9jmuscWstVytLvscuh6P9wPCrtd3A8Muz3cDw67Hdw9AOs/7p6A9Rd3j8D6h7tnYF1xYQ/MZMIeCAl8wCVXdxP3DCzCrS04djRvITk9I4fmQXIyisvSjmenRmEflhNw2SVJyRnOcDW5udXVWVCqq3NzWYWOb2VkJpUw8A/AibPpKcVWisLcqvKTUHsF+bVHXIuTsNr8gtTs42llWTVW5mQ6e9eQEWiyZcoLs8rzslPzKa4KC9/hiIjY2FOnEhMPl1Y4gdcWZOeVV1uQKzncYH/BDDBLUiyg1eXHrZgHQ6Ojo2PjrN8/OlQagfwt8VRRPEg4ZSsAQ4tOlaIvhh8pOJqWRRl0JTtopowx6Jk5lELTsgsqSm119YFQqMKKAwDYNJmIohbSMnD4aE8YuofICOrJkdS8KtKk0+lcF3UkVSbRGe91F/AkmaQ6WGXHC/YnFkWHx0RYaU9FUpQHwO9caUMi4QObdosS4LMESv1RcRGJv0vNI82i2AaMlVg9Nidpx5aNUbOfm5ZNfhmoKApeNcpmjQnhFrONOWh96RQ8gFxbRTE4zvpqaCwyEPLW4tCjhNDQkNrsMqTiYg66DtMxuuRwdkjLhWGp7Gi+jS/WqroQSn8Rzs5EHRBu1a3to2Uxh9BTNCWlCTHwkdUojpUX0pLhZTgkZTK9hE5pJ2lnXsilsZxgkDkC239SQBcOj4qLLXWmRf/saD/SsfXAsPiQKAtjFHmGRNvx2YiWjQjplMFiHMRL3xEtk5brRFsBLDqizNFaDcTY7Bb5U8yhQ6XQUKNsd3U6ijw0FMWQ0+h42+EkLQ75MuwehyM/Ye+Mttp5puPt84js1YprUyOijY+LOxBx6rTDjMSFHCTtOSzm9EGbBZNyDNKmQFN1DA9YOqTfSWhguNJGO0amkIoia/1lvYMIm/M7SGxEyCF00OnYA6SpOGxxQFroG7TKJCTckiToYJykEvjKTjyNQctyvnCRxS5tcvrwKRSfoh3ejwpxkQPQTtFXp2NjKmC0czoA0tJh6E0n3YyRTEvfty+DlpJEo2XugJZNq3K+MIqnwG0Z44Bv2uj91AcNUYKIdoxsiDYT1hwcHE+i4TiWTKNhGI2WgtS9A1Nwo0W+DvZbn1jn/xCwLvIg1bl9axeqlAptYZSOI+xvQVpIBq2WToNRK7mYxmTQilMA9DtmAGjDHAPQYWCJXaW2G/BEGxZOHkKOI4PKQft7x2jplB4p2uQcTklGZso+qG/G9mk5tDKXSyc46GY/CCc9vyLBkgf270cTHl3hnDP2I9MIiyS9z2luPNAmV6akULQ7iGHutChzWqf9EIhMOJCYWJQQa3khtqjowIG4uLhQh/AVVxQVjvCiyIlwCWAhqSStzRKS6cUZdEi7M0vg0MpdaCMcMmoYdJ/9iYmlPhcPK/YnRsTBoFcUah0e7vAupC0mw5WFFqY1RuZOvazEifbwgVgy9x4M2YEcggkkNDrGxa4hLRWuLLSQE89MQUFt+7DOtChpHj7t7kVHYEOTeiz76PHjx/NIgQ+OZh9LTS1w734OlZ4qCo22hzBIy0SR1koLcnL2ZaL+pOR9aZFbHzoMoiizOwJ7rry0qmrnbtFVCnOzytLyjqbmu3HbaPfBGJaBM+mQls3ex4E/0DZydpJ5nS0hNhxERZ8Kq009nlaV61iQZhSnZ6akkOmTToe/KitTkjPTi526dlZV2tHUWk+0yEqL7SU4lrLDxOtCC7vBY3llLCtjekpSCfnJCu/jMZzJ4NCTUtKt4KyyvNQjLrT7SsgS3FLTMlA/XbkTWCfa/Oxyi0KLK+ls5janCuPaG/usvAI7bfE+5GJkZ8lhc5LIQ1J2UI1jbFjJ22ip/jo9ibNdTqdTMjmVJA+r1kqbgxpHtpPR0LcNCxhk05GbbaVlwSb1/RpSq+DsShqtwEqLJLOEkWRjTeZufQpnYSaRqPYZCwnJ3Ul28SYZVtqQAot9ZSTRk5Izkys52+14sRI0WayTDqjBoyU94ijZ/mYkMbdrAoBRicJkeaprOq3eSVXkHy2UsNRytFhWTN+OajEUmWlZRz3E8uDSUsBIw5kcP50Dp0NrLUxzPxGSrGDThqAFhjIUyuh++BkXmUCuJ7XuGi2U2uPI6ZIZvi2YCXsiWlWql3MEmjbHKy2UVKTgYrZ3XpK13McZAk6b7+tatXnQ5TJKPPNyUR1R7nP87tJCCz4Oy5EMjjsvluQH627TwhBxFBpwscs1AVolK9tybIBpaTS32tETL7SHSscAzIRNHOtoASz+LeLNdsMCGm+90uZnH08rr0L7QlVl5WlpMADn2C+LuxX7LLfxx1DtjeJ24GgBjdoNyssucKEud28/ku3jUOrKycmAxX86tRhVaI+3YQXZaVn2PiYngLQOBSKr6mS23RBR8KJVog/Gsdkc2IekZBY7LCzgOGbxO8CAtIV5lnuFTUKVBTQd1d62wwLFi3GZDvuDNVV5li6ICrZbZF9H1tpjaVTpnZNMZwd7awvW50nUNgyt+uQxNLH5J5G10r3zWliPkKRUO1OcFPw9OPv1uZaWAhLDtu0ICl5e93Ysej2SetLSzgSoSdie4GyqKSusOp4fdgztAqZ4qG6YcCIKT6Yer/qApFaBxKQl56alkgbsystNJo28hpr94O1n+i/cEnIvubCM3GVNcehYyAqB8qiUbXdEwRPAoFRc6NhV4tZGM5O+7Y4o6MK17NhCIf2NauKDuZ39noKxK6k0kgOLRybyqb2nVCcBTDoZKOhQuXvHUn0JzknO2faKyIcUsO//ASzxGiW8sYxlAAAAAElFTkSuQmCC",
                  height: 50,
                  width: 50,
                ),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            const Row(
              children: [
                Text("Dream Company : "),
              ],
            ),
            Row(
              children: [
                const SizedBox(
                  width: 40,
                ),
                Image.network(
                  "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJQA0AMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAACAAEDBAUGB//EAEMQAAIBAwIDBgMFBgIJBQEAAAECAwAEERIhBTFBEyJRYXGBBjKRFEKhwfAjUmKCsdGi4QcVM0NykrLC0jRT4vHyJP/EABsBAAIDAQEBAAAAAAAAAAAAAAEEAAIDBQYH/8QALREAAgIBBAEDAwMEAwAAAAAAAAECEQMEEiExQRMiUQVh8IGh0TIzkbEjQnH/2gAMAwEAAhEDEQA/AKBFKjxTV68+f2DimxRUJqBFSxT0qhBsUsU9KoQYihxR02KgQcUiKPFNigSwMU2KPFNihQbAxTkUWKaoGwcU1HTYoEAxT4oqWKgbBpUWKbFQgNNiipqgUIU9MKeoAt4ocb1JimrQxsA02KLFICoSxitNijpYqEsDFPinFPUJYGmlipAKYioSwKWKPFLFAlgYpsVJihI3qBsDFLFHihIqBsEimxR02KFBsDFKjIpsUKDYNKnIpYqEsE02KLFKoGwSKajoSKAS9imxR4pYrUVsDTQ4qWmIoBsDFLFHjalioSwAKfTRqu9UeK8Vg4a8cTI8ssp0pGmMk1WcowVyL48c8stsFbLeMU1RcOvbfiEJa3Y6k2eNtmQ+BFWQuTijFqSuLJOEoS2yVMDFBPKkEZeU4UA/0zT3tzDYxdpOwHgvVj4Vz1ncTcUaaWYZySiqDsoI/wA6wzZ1DhdjOm0ryre/6ToY2SaNZInEiMMhl5Gn01w/B+LT8NkUfNCw70R26Ly8DvXbWlxDewLNbOHQ/UetTBqI5V9y2s0U9O77iPimIqXT4UJWmBKyPFNipMU2KAbI8UsVJimxUoNkZFNipCKbFQNkeKapCKHFCi1g0xFHimqUEvYpYosU+K0FLApYosU+KhLBxT6aICgml7MLGoBllOEBOBnxJ6ChKSirZaEZTdRAmfswwTdwjNjyAya5K7QzfEHD4pH3XVI2d/HG/rmujxIYLmaTfUiIMr4uGP8A0Y965reT4luWKgfZ4AoyOp3/ADNc3NNzfPyd3SY1ii6+Hz+xdFqGmM8MrW9ymyyqM+zD7wrS4VxcXEn2S/Rbe96AHuS+an8qyhddiDqG3WliG8i0TAMAcjxB8Qaum4u4Gc4rJHblXHz5X58GdxG6mvb9zKc6X0qOgAatT4bgIiZdLMO1QnbAPLO+On68RmRRqJCzHus5x4uduX1rZ+Hy0jlimcOpWMDu7aT4dP15JN27fZ1VSiorpHPNbLJHHknbHeAx4c/pUVhe3PCpxIjY7oDA7qwA6/WrZXsjIMA6SVJIwNtXP6UpoFn1oVJBJG25G/48qz5TuPZraa2z6Z1vC+IwcSh1Rd2VR34yd1/yq4V8RivO0eawmE8bEHcq6eJ39+R2rs+Ccai4mvZvoS537oOzeY/tXU0+qU/bPs4Ou+nvF/yY+Y/6NDTTFamK0JWnTk7iHTSxUmmhdkjRnkZVRRkljgCqul2XVt0gNNMVxWDxPjTSao7PaPY6+rjy8By/5hTcB4vhvsV5ITp/2crHmD8uf70qtXjc9h0H9OzRxeo/8G8VpiKlIoSKaoRsi00xFSEU2KAbLYohTAUQFaCzFjPSn01Zs7ZrqVY0xpONTZ2ArlLX4gmk41cIiCS0acpEBsVUD5s+gGfOsJ6iEJbWOYdDmzY3kS4N64lFuqnGp2IVE/eY1VVezaWWR+0dI3LEHu5YhAB4bMT7USktdLJJviPXjoNtQA9sD1pdliyOpu9JIM566Qf/ACpbLk3v7DeDF6UfuRy5NmrOe9LMTttsqgY/EVzHCU+0XvF52DbvgFRnGK664XsrWLfJjgLEBfHLD/AV+lcdwOMjgT3Qc65ZnY42PT/OlJz5X6nUwY3tl+iK13OBBzFUbbiDRToobOpguPU1LxDToYqCNu8PzrHsyW4lbKesyf8AUKxyZWmqG8OCLi7OnADOrFts/XlyrV+Hl/ZS5IUDJK4GOScyR0/XhWfbprEcryEJkDUNyx2wFH6/Otjg+Ugulb9gqIp7Nc5jG2Nx12O438PGtK8sx3eEY16oXiF2Rt+2kHd+73z/AH/XOoue3gvTof0w/XO1xFez4ncZBjP2hsFhgY1KdwfWoEQkIGUJpxlPpyO3h/8AVZ0vBp12JkVsjYtk9Nm/sedZ727W7gxsyhTzzuuNs/0q8uyqXx3RzPkMb+HMUbcsMflGCT1xtv8AWp2RNrg7Lhk5uuHW07fM8alv+LG/45qwRmsz4ZcNw4x4wY5Dt677e+abiXGBCTDZgPNnGonYHGffbJ9q7KzxhjUpM8vLSTyaiWPGvJavr63soyZTqcjIRfmNcrxDiE145aQhYxuiKcDGPzGfdhQTOZH7SQlnYhwWO52/8fxNFHaPhTJmOMllV2U4JAzj2P4KK5efUTzcLhHoNJocemVvmRUWJ3kWNVJJfRscas529dzj+WgurR47czEuZIJTFOAhwmTtluXPVt4Yq72STFUQaO1TQEDrs4wATnkNgcD9071Xupu2fChYY3VUkhTuouBjJGeYGd/FawUUh62zW4Hxj5bW8OOiO3TyNdARXnaKULISutCdx/Wuo+HeJNLi0uDl8dxjz9DXS02o5UZHD1+iXOSBskU2KlYb0OKfo41lgDO1Wra3eQ6goIHy5OxPn5eNNZ25uJQo+Ubt6Vc4peR8N4VcXY7nZpiLfryB9Sce4wedI63V+n7Idv8AY6n0z6f6z9XJ/Sv3OLb4nu+GT8Q4bBi7ijhaLtTs3anZiPFchsDwxVL4Ws+0eRnGysULDmSTliPwrHjkGASSQe+Qxwy7+A59a7bglsIrJWYLhlMhITSWLeXQ4OfY0njik93k6upm9mxdE6AOk8hx3207HY76jt7L9asdkeyhjRcsU1YVepORz8tJ8vpUyRERRhFIZskADGCTjr6ZyfXnU8jQQzvNOdEKNvqbGlVH9QoJA8snerymLY8fX6IxvjCQW/COJNlwBH2QBO4Bwq/1GB1xk1g2sUVv8KWC7mZkMmSMEFmJx9MVS+NeLS3tvo78aNJlYV5776nPidjWteqY7aCDAKpEqjywBS2R8o6mmj7G/ls5PjUqvlxsSMEDrWRw1S3FrMD/AN5T9D/lWzxeJThQDrJwAOtPwvhHZYuLg6XGQD4cxt5+dU2uclRdzjji7NRJDGqkd6UAKWx3YhtlQKKDiMXDGuJHAkMiiRHXfTuwBPhtjJH4VDI40Iy5VNOpEHzP3Sf7UN9rSFO2iKItouSjY7M6s48q2yNpWhXFFN0xXbfaGmm1LqZS5yNIAKL45/dPLI55o8d+RSMgMTjOCuCwGD7/ANj0p5AE1s+AqvIVlUaVUKuF/wCA94bjbYezMpjlHb93Scl+WB3Cc+uTy7prJSaN3DgMlT+0Y4BOQ2Om53H05/hUeGQhWXDDn7DG3lvRxq6soOVbSATjboNx6A/kTyolIYY0E7auzzy8dPuRWqaYu04ihmlQdgHxFMdMmDzxn88fWpIoXnCoid5iBgDk2dx9e770CxqJAXYmPPeYc9uhHQ/2rXXuq6KqHtFOQuNQZWBJ5bZO+3PFFwb7IsiSpLkorbdgdJXvZKnUPlOrJHs3TwFDO5l7VmRQ0j6iFXAyOgA6Efiwq1OgDAiQFTh+6CADjDYB57aj54qOaNFkfQW09oChYYYr0O3LGRy/do0FSdlS6tmEhXs1btoxMio4YAc8HzODt/GKgdZGTvHET5ILNhdQwM+B+6f5jV/9mrqZY0dYZAey/eXIJ3Xw7vI/d8qjvIeyuJoQySOjF07BtSacEsoPkNQ5fdqjRonZlXyxhYZrdJdKoEkEjAkvucjH3fu7/u1HHI8TJLGxDowIbzHM1ceISFu1dQHQ6pGUgZHMjHnpP8x86oL3e7ICunKOPDHOjFkmk1yegWVyl7aRTx47w3HgeoqbTXMfC92YLhrWU4SY909Nf6xXWY6iuzhn6kLPJ6zD6OVrx4NS3QwwKDsz7nbJGf8ALwrlf9I92VtrbhqbtI2txq6DYb+pJ/lU12Iy0meo3wuBk9MdOfLzxXlnxDxFL3j15dBlkgtExHpHhsuPI4JHka86pvJkc5Hs1ijhwrHDwQ8Ntftc4iVCRJIsQYtpIVT3seNeioh7E6R3ie6NWM4wB+JO/PJ8eXMfDliE4gqSlWXh1sNe2SJZNznHgAR77V2cSDWivoIQHOThQRuxHvq36ZwN6aUvbYhmj79pC2iBy+gmKIZO5zhFON+p7vsT4muN4hey3VwhZzEE/wBnEoyEP5scn8663jbrDwubQra5FWMadickNy+78v8Aiya4oHU7aXOMaNEY3PXCfmx/tWkFZi+GZPFBHPf2VoSxPag9mm6qSR8zdW8a6a+ki+yvC6kSB8o/l4GuesZUk+MbSKV0MUG5SLGlcKzYz13xv41bluvtN89v10s2522BP5UrJbpUjqY2seJNlO9tbhL5IZEDorK4I9Ryq3LIxf5Q833UHyryG58d6lw0jaYHJYEqZjyTcbCq01xHaxlLcd9gxDDfJG/9qZhDYuRHJk9WfCBLLbu00uS4Ld4jPQD2HKld8R7SZ0c6WNmJDIqDODvyzgjfkfxoEs2YGa8woC7oSMc85P4fSjvws0rnKzW3+rgwjTaRDtyyORHqNt8bVlmcoxN9PGMpfNeR2QFyY9CGR9OOayao+Q8PlGx/Cg1N2bBFIfsyzQMe8mVUDDHl8p25H6VFE7QIJ4XMlslvDM2wL7dGU+RO/wCPSpU0COJGUOY4kPZocsoDhsBsfu/dPtml1KxpxonkXWZuy+Us+VO2N2GMHl83oaRIB194pnIXJBAOT9dl/LNQszQKWkkZjEHAnUbhgwUKR7DnkbVDa8Qiur2KGGNSJFljRySoIHyjHQd0HFX3JGTg2aCNtkth8Y7Qb5I2ww9d/wC1a8KqYg6BJDGVYjdkA+U55E8wfrWFFIXwWBSfCllYfMxUE+3eP09q2+GzK9siTLlIlKIH5ICDgjTvyJPrzpiMrQnkhtdkcix6dLAnvc87lcArhem2B/NUfZt2IKspYthkDd9sc9Wdhk6se1WWJlfLA9/SHZ8Ek7YwcfvFR6LQLGqglup048Ttp9OSnPmasRMqy5xpaNVMfdCKoXVjOQcc8nX5nUKgm7TKhmYvHgITyGMY/HB/mPnVtlOGz+zGQAoJO+QF/wCw5Piagl7qjLMqldDRrz0f3xv/ACeNAuirNHuVUBlwAqKcqRg7beRZfYDrWfcALIJNSkN3H8dtwfcY8sg1rlAqqszjIbPdBBxkHOeWzb+PeJ8KpX08j2EltgLC57VImQbOdh3uexDL4Y96zfBtHkrxEroddiMHboSc13fCbxb+ySbIL7LIB0avPLSYz2+rfVuGH7rcv16+tb/AL37JeAOcQzHSxPQjlTmmy7ZfZnM+o6b1Mb+Ud3xy8/1dwW6ugcPpKx+bHuj33+hB6GvL7G27c20ejs/tEhuXyf8AdxgYz7gfjXV/6Sbky/ZOGQrqVsSSeRbuoTg9AXP82/KubbW93cQQBmyycNtzHuR+9jx/+Jrkx4R35cys7P4RQ/6oiu5srJeTNOCy/d+7/hUb8zjAroEQqWI0NjSudOWzzI0+Oy4HIb0FrAlqkVtAXRYY1iVBuxIA6+ICjJ5AE4qzEo7MO6ALpY4iyWK53C/wkDOrrmmbSRy3723+cnOfE/8A6eCBY+87s7IG2IG2Wb1yD6Vy4OtcltYlYhey2eYcgq+C4xvW58UT/wD9zQxsJjbxj9mDhFYfMWPhqyfPNYS923LozQo64abHfkP7iDoPP1piKqIv3Jmb8PRseOXlzJEiIQ0apGNlJIAq0sdos/byzqq3JKpq5uMLkeXOq3BbSaBXWFiHkw0h1EgAZPP9c6qfFfZxyWFnFhhGhYvnkWO//T/SsKcI72h5yWWfpJmnPdvORBZqdJGwG3U7/hSaO3sY2knYSS4ZkHXcZIHsOdY3AL94LdkQBmyu7ckGNyasIlzeTGKAGW4BGGz0P9Birxy7knXJlLA4Om6ihry7a4eVmB0aVHYjoSRz+lS3YeK9tD2DIpscLMqkGMhGJG2x5VrW/D7XhiG8vXUMX7xY5C48B1qnev2/GYbmzuVK3Fmc27bGTKuF25Nv051nqIOMbl2baXLGc6gval39ynFcCVDcMxEz2TKLxAcZB6r49dvoaK7WNY5pSFj1xQSmRTiKbB04GOQ3I223HKq9gIpxwsRsLVjctHJA7HS65QsAT68j486CGaRrZYZ4dDTK1qtpICFZ10FeeMEl8c+nOk7Hy/cSO73MNwrpOz3ESEjGwCkY/eGd99/OsSC4W0vY5wCBBeB99iEO+PoD9a3oyLi50qC7G4jZ4pNmTtYzq0E89xn61nXNvFPZagC0SwDGcCRSsmk5HUYY79MdM0bBRsGNBK0Ew0vHIVTDbAK5XY+PfGxq3w67S10peFREyae2BwFwASXHTdWHpWRw7ikl/BedtahyssIlVSdwymNn8jqCn396uI2VMsT9tASWKnHaJ3tx5qAXpmEr5Qnlh4kdMwVVimgcK4AxIMh8nYkLy2yT7daCdYm1C2JUOobWQA3IBcjoR3fpWFazPBh4T2kRySp235MQTyPz7VqwXMV1GZYyNSSHUXwGAY4Ix5ZO/wDDWqZhtp8CnTXLmMOrPhtfJiCABlfHBH0qmwaQuwCK3znWd235efe1jzzVu5k1K8kmWkRgw7Tqh5j6ax7VFcQEySIhGraSMMNi3/6APo1Sy1eTPI1I7sxZPlcud8Y3P/Jg+q+VQXUZIZX1M+CxwMjoG/7W/Rq05AJEGe+owrDbVzA+mpfoKjIJUdlkhCFU6sq+BsM9dSEg+npQZdIwNL23EQJAgE572MYDjn9dj71fiJOMc9uvXc0HGLPWrCImTUwEMifexkoceYyPX1obG4+02wlU97G+/I43/L9ZqY+6DlVrcW+McTXifxBe8SEh0QhmTVyI5IceJAyfMmtf4MtDccZ4fDdI7C0hN3MynSA7bDUfc1x6L2qRRkAtcy6mx+4v6/E16V/o4tJDw++4ohbtbqbRG2MqoTuqcdTksfelV2NZHUGdadTI7rpbmVU7bZwurwXYHHWpYoVCAJ2kaOVRNfzOgwB6KAOXlnxqUQJE0cZCxqjatGdkVR949eSjHmKj4zKFtmeT9nCkLSCRjgsxGkZ8ipk2/hq263Qrspbjzb4jm7We7ZysfaS5WJDu24Gpj0HgPrWdE/26YzTygdkmpt8aR4Y6DcfSm41Ky2qCBSqyuHM0hy8oUHfHhnGKqcKsO3JXtCsWdDsTu+B4egz707uakkhKMV6bm2XpZ5Ji1pw1NKAsrP8AQD8q57ili8vFnV3IjhUJrxjXgb4+prauuJrBGLXhYyxCHtAM76t9up2qxYcMjWKS+4pKBoIPebYBmx3vbpUyQWT23/AcWT0Fva/8+WUeE8Ea6YLChgs2Co0mckt83ucAVpXvErDgdsUtVSWdGbUvXO4Go1R418SPKLi34aOzRZARKNiQRjYdBiubnLMJuyBeUzKOWd/LxNUllhiVY+zWGDLqHuzcL4/kt8RvJbg3L3cj9n3SqjoM9B6/o1aUWst5wVHV0ke2Ts2VsrjLAqw/MfSsqbs43u5GxIxKZQjYYxzNacd5ouuArLbxyK6R6WK4KNrPIj+lJTk5cs6MYqKSiQPO8FlZz8Q/b9jcMY54nyeSEHPJt1OQd/SrUzLaO5k03MVpxMacHvQrk7eK7oNjsf6ULcNFwWf/AFc32lO2R5EaPcLpb5l/MedW7hVM3FfsUrQT60mIkcBSSTjB8DrOx8t6zNR47No1W1hkMqLePE8irh4iNOj6MD4jc1KLj7WFJBLXBuVjdSdUMZ/eHXBz9Bv0ITzwre3coUW0sN1DdsSxw2Qdj4buATuPIVY7FVu0hmDROtxPBE6DuupGQT677jqOXgQGTwYpJxC7hV2AuLV9JXmGGHGOX3lrXyWL3Fu6o7ajG6bLiQBxkdDkfgaz4Fhsr+0vJx2fZxLI7pur97Q2PPfNWyjwHBxGy5BKnut2bk9OXdZtq0x8MzyLgumaN521FYJSxILf7OQbNk+ZXP1PsTBu1KyExXBBGBzIIIPqMFzQK1u2qC7VUOhtm5bHRt4ZVwPamLPFGVnUzwb7Y7ynRvj1II/mppoTun8fnybttdG5iEsvZ9oSYmQDuknDL6bZGPE00bERK2y9g/ZvvnK5Az57FD61Q4fMIhdIxV106o2O2JF7wXw5lhn0rYdEW8WEgG2uocAKQV2G+D5o3+A1nuXk12O+DJn+9pDLJq7VVbkCW2Hs6nf+KhKCMrMo0iQBQo+UEnK58O9lfLI8KslzhY5X7MMxE+r5ThtJPswVvSodSIskLBlYku2d1XJwfowzjz86N8AS5AnWOa3MMVusAiQKJkbnqJIJ8CrbbdMVzoP2PiLRdoCk4LcsASjmP14107QkNa3HFC9vAyGaZ4hr0q2VcaR1DAHHP61h8XlSfhiWr2Ua3izGUXKEkllX5OWCrKAQfKqbknZqof8AXwZ4Oi4l0/7q17nlXtXwnGI/hfgscZKDs0AK817mrI96alWUi8vBvWkaS391HIoZLREEQP8AEzZz4/Iv0rO+I20cOvHIDl1QHWM5GknHpmlSox/uIxn/AG2eKcVvri5/bSyEuWxt0GtR/SqduOz0FebHc9SDq29KVKt5N7yY0li4On4VaQ2/D47hFzKwbLN5INvxNc3f39xfMwmfCdko0LsPnAzjxpUq2y8Y1QnpPdmm5fnZRnJzcKTkLMg9d6K9/ZrIEGnVOAxHMgg0qVIvs6iAnOiO5kX5tY578iK0ftc8dxwBQ5KyRR61bcE9od/XzpUqrIkeiHiNtHZ2c7WxePvwSKAx7p/aDY86s3L/AGqLiayKoZbeI61GGbZG38d6VKg+yy6CiX7U8ltOS8UlpCcH7veQbH8vGlLO9vxG5C4dftttlXGQSUck+RJ32pUqAQ7tfshFuhLRGK4yjnIONxn60zsYrtQnyTyqZFPJtUQJpUqv5KLlGiYknRVkGQ8Sk+OWTB396r2k8n2x4ScrqYjPMfKf6mlSp2Xg5+LmErHz2YV0ADEsucdAD/4DPvWnb3EnaWqau6k+tP4SNsDyIJFKlWM/IzDpFniMKDi91ABiOURu48SylWPvge4q3xtzaXjW1tiOG4W3jlUfeWRRq/HBHmKalWcejSXZkwALeTQYzG8UEjA9S3db6j+gqtdRBuCySEnXCJOzbqDFJhT6460qVBlz/9k=",
                  height: 50,
                  width: 50,
                ),
              ],
            )
          ],
        ),
      ),
    );
    /*child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      height: 30,
                      width: 600,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ),
                      ),
                      child: const Text("Portfolio"),
                    ),
                  ],
                ),
                const Row(
                  children: [
                    Text("Name : Namrata Chaudhari"),
                  ],
                ),
                const
              ],
            ))*/
  }
}
