// ignore_for_file: camel_case_types, non_constant_identifier_names

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        home: indian_flag(),
        debugShowCheckedModeBanner: false,
        title: "26th January..");
  }
}

class indian_flag extends StatefulWidget {
  const indian_flag({super.key});

  @override
  State<indian_flag> createState() {
    return _indian_flag();
  }
}

class _indian_flag extends State<indian_flag> {
  int Wno = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Happy Republic Day...."),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (Wno == 5) {
              Wno = -1;
            } else {
              Wno++;
            }
          });
        },
        child: (Wno <= 4) ? const Text("Next") : const Text("Reset"),
      ),
      body: Center(
        child: SizedBox(
          height: 450,
          width: 350,
          //color: Colors.black,
          child: Row(children: [
            Row(
              children: [
                Column(
                  children: [
                    (Wno >= 0)
                        ? Container(
                            height: 400,
                            width: 10,
                            color: const Color.fromARGB(255, 69, 43, 33))
                        : Container(),
                  ],
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        (Wno >= 1)
                            ? Container(
                                height: 50,
                                width: 200,
                                color: Colors.orange,
                              )
                            // ignore: prefer_const_constructors
                            : SizedBox(
                                height: 50,
                                width: 200,
                              )
                      ],
                    ),
                    Row(
                      children: [
                        (Wno >= 2)
                            ? Container(
                                height: 50,
                                width: 200,
                                color: Colors.white,
                                child: (Wno >= 3)
                                    ? Image.network(
                                        "https://cdn-icons-png.flaticon.com/128/3336/3336064.png")
                                    : Container(),
                              )
                            : const SizedBox(
                                height: 50,
                                width: 200,
                              ),
                      ],
                    ),
                    Row(
                      children: [
                        (Wno >= 4)
                            ? Container(
                                height: 50,
                                width: 200,
                                color: Colors.green,
                              )
                            : const SizedBox(
                                height: 50,
                                width: 200,
                              )
                      ],
                    )
                  ],
                )
              ],
            ),
            Container(
              height: 350,
              alignment: AlignmentDirectional.bottomCenter,
              child: (Wno >= 5)
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Image.network(
                          "https://cdn-icons-png.flaticon.com/128/2090/2090228.png",
                          height: 40,
                          width: 40,
                        ),
                        Image.network(
                          "https://cdn-icons-png.flaticon.com/128/2090/2090228.png",
                          height: 40,
                          width: 40,
                        ),
                        Image.network(
                          "https://cdn-icons-png.flaticon.com/128/2090/2090228.png",
                          height: 40,
                          width: 40,
                        ),
                      ],
                    )
                  : Container(),
            )
          ]),
        ),
      ),
    );
  }
}
