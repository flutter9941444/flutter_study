import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Happy Republic Day.....",
              style: TextStyle(
                fontStyle: FontStyle.italic,
              )),
          backgroundColor: Colors.purple,
        ),
        body: Center(
          child: SizedBox(
            height: 450,
            width: 350,
            //color: Colors.black,
            child: Row(children: [
              Row(
                children: [
                  Column(
                    children: [
                      Container(
                          height: 400,
                          width: 20,
                          color: const Color.fromARGB(255, 69, 43, 33))
                    ],
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 50,
                            width: 200,
                            color: Colors.orange,
                          )
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            height: 50,
                            width: 200,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.network(
                                      "https://cdn-icons-png.flaticon.com/128/3336/3336064.png"),
                                ]),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            height: 50,
                            width: 200,
                            color: Colors.green,
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
              Container(
                  height: 350,
                  alignment: AlignmentDirectional.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Image.network(
                        "https://cdn-icons-png.flaticon.com/128/2090/2090228.png",
                        height: 40,
                        width: 40,
                      ),
                      Image.network(
                        "https://cdn-icons-png.flaticon.com/128/2090/2090228.png",
                        height: 40,
                        width: 40,
                      ),
                      Image.network(
                        "https://cdn-icons-png.flaticon.com/128/2090/2090228.png",
                        height: 40,
                        width: 40,
                      ),
                    ],
                  ))
            ]),
          ),
        ),
      ),
    );
  }
}
