import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ChangeColor(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ChangeColor extends StatefulWidget {
  const ChangeColor({super.key});

  @override
  State<ChangeColor> createState() => _ChangeColorState();
}

class _ChangeColorState extends State<ChangeColor> {
  int box1 = 0;
  int box2 = 0;
  final List<MaterialColor> colorList1 = [
    Colors.blue,
    Colors.pink,
    Colors.lightGreen
  ];
  final List<MaterialColor> colorList2 = [
    Colors.green,
    Colors.amber,
    Colors.orange
  ];
  Color changeBox1() {
    return colorList1[box1];
  }

  Color changeBox2() {
    return colorList2[box2];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Change Box color")),
        body: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Column(children: [
            const SizedBox(
              height: 10,
            ),
            Container(height: 100, width: 100, color: changeBox1()),
            const SizedBox(
              height: 5,
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (box1 < 2) {
                      box1++;
                    } else {
                      box1 = 0;
                    }
                  });
                },
                child: const Text("Button 1"))
          ]),
          Column(children: [
            const SizedBox(
              height: 10,
            ),
            Container(height: 100, width: 100, color: changeBox2()),
            const SizedBox(
              height: 5,
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (box2 < 2) {
                      box2++;
                    } else {
                      box2 = 0;
                    }
                  });
                },
                child: const Text("Button 2"))
          ]),
        ]));
  }
}
