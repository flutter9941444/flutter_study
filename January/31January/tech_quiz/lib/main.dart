import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int quizNo = 1;
  int result = 0;
  bool correctA = false;
  bool correctB = false;
  bool correctC = false;
  bool correctD = false;
  final List<String> questions = [
    "What is Flutter?",
    "Who developed the Flutter Framework and continues to maintain it today?",
    "Which programming language is used to build Flutter applications?",
    "How many types of widgets are there in Flutter?",
    "Which function will return the widgets attached to the screen as a root of the widget tree to be rendered on screen?"
  ];
  final List<String> optionA = [
    "Flutter is an open-source backend development framework",
    "Facebook",
    "Kotlin",
    "2",
    "main()"
  ];
  final List<String> optionB = [
    "Flutter is an open-source UI toolkit",
    "Microsoft",
    " Dart",
    "4",
    "runApp()"
  ];
  final List<String> optionC = [
    "Flutter is an open-source programming language for cross-platform applications",
    "Google",
    "Java",
    "6",
    "container()"
  ];
  final List<String> optionD = [
    "Flutters is a DBMS toolkit",
    "Oracle",
    "Go",
    "8+",
    "root()"
  ];
  final List<String> correct = ["B", "C", "B", "A", "B"];
  bool displayResult = false;

  ButtonStyle giveColor(istrue) {
    if (istrue) {
      return const ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(Colors.lightGreen));
    } else {
      return const ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(Colors.deepPurple));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepPurple,
          title: const Text(
            "Tech Quiz",
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
          centerTitle: true,
          //backgroundColor: const Color.fromARGB(255, 188, 65, 250),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.deepPurple,
          onPressed: () {
            setState(() {
              if (quizNo < questions.length) {
                quizNo++;
              }

              correctA = false;
              correctB = false;
              correctC = false;
              correctD = false;
            });
          },
          child: const Text("Next"),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 15,
                ),
                Text("Question : $quizNo / 5",
                    style: const TextStyle(fontSize: 20)),
                const SizedBox(
                  height: 10,
                ),
                Text("Question : ${questions[quizNo - 1]}",
                    style: const TextStyle(fontSize: 20)),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "A") {
                      result++;
                      setState(
                        () {
                          correctA = true;
                        },
                      );
                    }
                  },
                  style: giveColor(correctA),
                  child: Text("A . ${optionA[quizNo - 1]}"),
                ),
                //style:const ButtonStyle(shadowColor:MaterialStatePropertyAll())),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "B") {
                      result++;
                      setState(() {
                        correctB = true;
                      });
                    }
                  },
                  style: giveColor(correctB),
                  //style: (correctB) ? giveColor(correctB) : giveWrong(correctB),
                  child: Text("B . ${optionB[quizNo - 1]}"),
                ),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "C") {
                      result++;
                      setState(() {
                        correctC = true;
                      });
                    }
                  },
                  style: giveColor(correctC),
                  //style: (correctC) ? giveColor(correctC) : giveWrong(correctC),
                  child: Text("C . ${optionC[quizNo - 1]}"),
                ),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "D") {
                      result++;
                      setState(
                        () {
                          correctD = true;
                        },
                      );
                    }
                  },
                  style: giveColor(correctD),
                  //style: (correctD) ? giveColor(correctD) : giveWrong(correctD),
                  child: Text("D . ${optionD[quizNo - 1]}"),
                ),
                const SizedBox(height: 30),
                (quizNo == questions.length)
                    ? ElevatedButton(
                        child: const Text("Result"),
                        onPressed: () {
                          setState(() {
                            displayResult = true;
                          });
                        },
                      )
                    : Container(),
                const SizedBox(
                  height: 30,
                ),
                (displayResult)
                    ? SizedBox(
                        child: Text("$result / 5",
                            style: const TextStyle(fontSize: 20)),
                      )
                    : Container(),
              ],
            )
          ],
        ));
  }
}
