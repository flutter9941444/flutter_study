import 'package:flutter/material.dart';

class Assign2 extends StatelessWidget {
  const Assign2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      padding: const EdgeInsets.all(30),
      height: 100,
      width: 100,
      decoration: const BoxDecoration(
        color: Colors.green,
        border: Border(left: BorderSide(color: Colors.red, width: 5)),
      ),
      child: const Text("Hello"),
    )));
  }
}
