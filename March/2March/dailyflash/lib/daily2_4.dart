import 'package:flutter/material.dart';

class Assign4 extends StatelessWidget {
  const Assign4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      padding: const EdgeInsets.all(20),
      height: 200,
      width: 200,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
        border: Border.all(color: Colors.black, width: 5),
      ),
      child: const Text("Namrata"),
    )));
  }
}
