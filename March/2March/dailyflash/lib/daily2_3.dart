import 'package:flutter/material.dart';

class Assign3 extends StatelessWidget {
  const Assign3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      padding: const EdgeInsets.all(30),
      height: 100,
      width: 100,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: const BorderRadius.only(topRight: Radius.circular(10)),
        border: Border.all(color: Colors.blue, width: 5),
      ),
      child: const Text("Hello"),
    )));
  }
}
