import 'package:flutter/material.dart';

class Assign1 extends StatelessWidget {
  const Assign1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      height: 200,
      width: 200,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.red, width: 2),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Center(child: const Text("Hello")),
    )));
  }
}
