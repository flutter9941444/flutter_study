import 'package:flutter/material.dart';

class Assign5 extends StatefulWidget {
  const Assign5({super.key});
  @override
  State createState() => _Assign5();
}

class _Assign5 extends State {
  String msg = "Click Me!";
  bool isClicked = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: GestureDetector(
      onTap: () {
        setState(() {
          if (isClicked) {
            msg = "Container clicked";
            isClicked = false;
          } else {
            msg = "Click Me!";
            isClicked = true;
          }
        });
      },
      child: Container(
          padding: const EdgeInsets.all(20),
          height: 100,
          width: 200,
          color: Colors.lightGreen,
          child: Text(msg)),
    )));
  }
}
