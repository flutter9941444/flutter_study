/*Create a Screen, in the center of the Screen display a Container with
rounded corners, give a specific color to the Container, the container
must have a shadow of color red.*/
import 'package:flutter/material.dart';

class Assign5 extends StatefulWidget {
  const Assign5({super.key});
  @override
  State createState() => _Assign5();
}

class _Assign5 extends State<Assign5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Assignment5"),
          centerTitle: true,
        ),
        body: Center(
            child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
              color: Colors.blue,
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10)),
        )));
  }
}
