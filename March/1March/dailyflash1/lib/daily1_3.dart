/*Create a Screen that will display an AppBar. Add a title in the AppBar
the app bar must have a round rectangular border at the bottom.*/

import 'package:flutter/material.dart';

class Assign3 extends StatefulWidget {
  const Assign3({super.key});
  @override
  State createState() => _Assign3();
}

class _Assign3 extends State<Assign3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Assignment3"),
          centerTitle: true,
          /*shape: const BeveledRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(18),
                  bottomRight: Radius.circular(18)))*/
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadiusDirectional.only(
                  bottomEnd: Radius.circular(20),
                  bottomStart: Radius.circular(20)))),
    );
  }
}
