import 'package:flutter/material.dart';

class Assign1 extends StatefulWidget {
  const Assign1({super.key});
  @override
  State createState() => _Assign1();
}

class _Assign1 extends State<Assign1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment1"),
        centerTitle: true,
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.favorite)),
        ],
      ),
    );
  }
}
