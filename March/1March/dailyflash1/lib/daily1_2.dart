/*Create an AppBar give a color of your choice to the AppBar and then
add an icon at the start of the AppBar and 3 icons at the end of the
AppBar.*/
import 'package:flutter/material.dart';

class Assign2 extends StatefulWidget {
  const Assign2({super.key});
  @override
  State createState() => _Assign2();
}

class _Assign2 extends State<Assign2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment2"),
        centerTitle: true,
        backgroundColor: Colors.deepPurple,
        leading: const Icon(Icons.menu),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.notifications)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.favorite)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.person))
        ],
      ),
    );
  }
}
