import 'package:flutter/material.dart';

class GiveFeedback extends StatefulWidget {
  const GiveFeedback({Key? key}) : super(key: key);

  @override
  State createState() => _GiveFeedback();
}

class _GiveFeedback extends State<GiveFeedback> {
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController feedback = TextEditingController();
  String? _selectedfeed;
  List<String> feedtype = [
    "About Tolls",
    "About Banks",
    "About Wallets",
    "About AutoPay+",
    "Other"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF622774), // Start color
              Color(0xFF0D324D), // End color
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: Container(
            width: 300,
            margin: const EdgeInsets.only(top: 150, left: 20, right: 20),
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: const [
                BoxShadow(color: Colors.grey, blurRadius: 4, spreadRadius: 4)
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(" Name : ", style: TextStyle(fontSize: 20)),
                const SizedBox(height: 2),
                TextField(
                  controller: name,
                  decoration: const InputDecoration(
                    hintText: "Enter your Name",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(color: Colors.deepPurple),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                const Text(" Email : ", style: TextStyle(fontSize: 20)),
                const SizedBox(height: 2),
                TextField(
                  controller: email,
                  decoration: const InputDecoration(
                    hintText: "Enter your Email Address",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(color: Colors.deepPurple),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                const Text(" Enter feedback type : ",
                    style: TextStyle(fontSize: 20)),
                const SizedBox(height: 5),
                Container(
                  padding: const EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: DropdownButton<String>(
                    value: _selectedfeed,
                    onChanged: (String? newValue) {
                      setState(() {
                        _selectedfeed = newValue!;
                      });
                    },
                    items: feedtype.map((String feed) {
                      return DropdownMenuItem<String>(
                        value: feed,
                        child: Text(
                          feed,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ),
                const SizedBox(height: 10),
                const Text(" Feedback : ", style: TextStyle(fontSize: 20)),
                const SizedBox(height: 2),
                TextField(
                  controller: feedback,
                  decoration: const InputDecoration(
                    hintText: "Give your feedback here",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      borderSide: BorderSide(color: Colors.deepPurple),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                GestureDetector(
                  onTap: () {
                    // Handle submit action
                  },
                  child: Container(
                    margin: const EdgeInsets.only(left: 110),
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.deepPurple,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: const Text(
                      "Submit",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
