import 'package:cloud_firestore/cloud_firestore.dart';

int totalTolls = 0;
Future<void> fetchData() async {
  // You can directly use Firestore without initializing again
  final QuerySnapshot querySnapshot = await FirebaseFirestore.instance
      .collection('users')
      .where('username', isEqualTo: 'namrata')
      .limit(1)
      .get();

  if (querySnapshot.docs.isNotEmpty) {
    final userData = querySnapshot.docs.first.data() as Map<String, dynamic>;
    totalTolls = userData['total_toll'];
    print("***************************");
    print('Total Tolls for Namrata: $totalTolls');
    print("***************************");
  } else {
    print('No data found for the username Namrata.');
  }
}
