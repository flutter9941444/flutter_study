// ignore_for_file: file_names
import 'package:flutter/material.dart';

class FAQ extends StatelessWidget {
  const FAQ({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FAQPage(),
    );
  }
}

class FAQPage extends StatelessWidget {
  final List<QuestionAnswer> faq = [
    QuestionAnswer(
      question: 'What is the purpose of the app?',
      answer:
          'The app is designed for automated toll collection at designated toll booths.',
    ),
    QuestionAnswer(
      question: 'How does the automated toll collection system work?',
      answer:
          'The system uses RFID technology or license plate recognition to identify vehicles and automatically deduct toll charges from the user\'s account.',
    ),
    QuestionAnswer(
      question: 'Who can use the app?',
      answer:
          'The app is available for anyone who wishes to use automated toll collection services.',
    ),
    QuestionAnswer(
      question: 'How do users register for the app?',
      answer:
          'Users can register by providing their personal information and creating an account within the app.',
    ),
    QuestionAnswer(
      question: 'Is there a requirement for user authentication or login?',
      answer:
          'Yes, users need to authenticate themselves by logging into their account to access the app\'s features.',
    ),
    QuestionAnswer(
      question: 'What information is needed for registration?',
      answer:
          'Users need to provide their name, contact details, and create a password for registration.',
    ),
    QuestionAnswer(
      question: 'Why is bank information required?',
      answer:
          'Bank information is required for payment processing and to facilitate transactions for toll charges.',
    ),
    QuestionAnswer(
      question: 'How is the bank information stored and protected?',
      answer:
          'Bank information is securely stored using encryption techniques and protected against unauthorized access.',
    ),
    QuestionAnswer(
      question: 'Can users update their bank information?',
      answer:
          'Yes, users can update their bank information at any time through the app\'s settings.',
    ),
    QuestionAnswer(
      question: 'What is the wallet feature for?',
      answer:
          'The wallet feature allows users to preload funds for convenient and quick toll payments without the need for frequent bank transactions.',
    ),
    QuestionAnswer(
      question: 'How can users add funds to their wallet?',
      answer:
          'Users can add funds to their wallet using various payment methods such as credit/debit cards or bank transfers.',
    ),
    QuestionAnswer(
      question:
          'Are there any transaction fees or charges associated with the wallet?',
      answer:
          'Yes, there may be transaction fees or charges associated with adding funds to the wallet, depending on the chosen payment method.',
    ),
    QuestionAnswer(
      question: 'What types of notifications will users receive?',
      answer:
          'Users will receive notifications regarding account updates, toll transactions, and important announcements related to the app.',
    ),
    QuestionAnswer(
      question: 'Can users customize their notification preferences?',
      answer:
          'Yes, users can customize their notification preferences and choose which types of notifications they want to receive.',
    ),
    QuestionAnswer(
      question:
          'How are notifications delivered (e.g., push notifications, email)?',
      answer:
          'Notifications are typically delivered as push notifications directly to the user\'s device, ensuring timely updates.',
    ),
    QuestionAnswer(
      question: 'Where are tolls collected from?',
      answer:
          'Tolls are collected from designated toll booths located on highways, bridges, or other toll roads.',
    ),
    QuestionAnswer(
      question: 'How are tolls calculated?',
      answer:
          'Tolls are calculated based on factors such as vehicle type, distance traveled, and any applicable discounts or toll rates.',
    ),
    QuestionAnswer(
      question: 'Are there any discounts or loyalty programs available?',
      answer:
          'Yes, there may be discounts or loyalty programs available for frequent users or specific categories of vehicles.',
    ),
    QuestionAnswer(
      question: 'How is user data protected?',
      answer:
          'User data is protected using industry-standard security protocols and encryption techniques to ensure confidentiality and integrity.',
    ),
    QuestionAnswer(
      question:
          'What security measures are in place to safeguard transactions?',
      answer:
          'Transactions are safeguarded using secure encryption methods and stringent authentication processes to prevent unauthorized access or fraudulent activities.',
    ),
    QuestionAnswer(
      question: 'Is the app compliant with relevant privacy regulations?',
      answer:
          'Yes, the app complies with relevant privacy regulations and follows best practices to protect user privacy and data rights.',
    ),
    QuestionAnswer(
      question: 'What should users do if they encounter issues with the app?',
      answer:
          'Users can contact customer support for assistance or refer to the app\'s troubleshooting guide for common problems and solutions.',
    ),
    QuestionAnswer(
      question: 'Are there any common problems and solutions?',
      answer:
          'Yes, users can refer to the app\'s troubleshooting guide or frequently asked questions (FAQs) section for common problems and solutions.',
    ),
    QuestionAnswer(
      question: 'How can users get support or assistance?',
      answer:
          'Users can reach out to customer support via email, phone, or in-app messaging for assistance with any issues or inquiries.',
    ),
    QuestionAnswer(
      question: 'Is there a way for users to provide feedback or suggestions?',
      answer:
          'Yes, users can provide feedback or suggestions through the app\'s feedback feature or by contacting customer support directly.',
    ),
    QuestionAnswer(
      question: 'How are user suggestions and feedback handled?',
      answer:
          'User suggestions and feedback are carefully reviewed and considered for future app improvements and updates to enhance user experience.',
    ),
    QuestionAnswer(
      question: 'Are there any user guides or tutorials available?',
      answer:
          'Yes, user guides and tutorials are available within the app to help users navigate its features and functionalities effectively.',
    ),
    QuestionAnswer(
      question:
          'Where can users find more information about the app and its features?',
      answer:
          'Users can find more information about the app and its features on the official app website, help center, or within the app itself.',
    ),
    QuestionAnswer(
      question: 'Are there any contact details for further inquiries?',
      answer:
          'Yes, users can contact customer support for further inquiries or assistance via email, phone, or in-app messaging.',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      width: double.infinity,
      height: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFF622774), // Start color
            Color(0xFF0D324D), // End color
          ],
        ),
      ),
      child: ListView.builder(
        itemCount: faq.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              _showAnswerDialog(context, faq[index]);
            },
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  faq[index].question,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void _showAnswerDialog(BuildContext context, QuestionAnswer qa) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(qa.question),
          content: Text(qa.answer),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Close'),
            ),
          ],
        );
      },
    );
  }
}

class QuestionAnswer {
  final String question;
  final String answer;

  QuestionAnswer({required this.question, required this.answer});
}
