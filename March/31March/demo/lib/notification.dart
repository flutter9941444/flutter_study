import 'package:flutter/material.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);
  @override
  State<Notifications> createState() => _NotState();
}

class _NotState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF622774), // Start color
              Color(0xFF0D324D), // End color
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // First Notification Card - Welcome Message
              _buildNotificationCard(
                title: 'Update Your Information and Payment to Wallet',
                message:
                    'Keep your information up-to-date and make a payment to your wallet.',
              ),

              const SizedBox(height: 20),
              // Second Notification Card - Update Information and Payment
              _buildNotificationCard(
                title: 'Welcome to Toll Collection System!',
                message: 'We are excited to have you on board.',
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Helper method to build a notification card
  Widget _buildNotificationCard(
      {required String title, required String message}) {
    return Card(
      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 8),
            Text(
              message,
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
