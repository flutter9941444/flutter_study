import 'package:flutter/material.dart';

class Terms extends StatelessWidget {
  const Terms({super.key});
  final String terms = '''
    Welcome to our Automated Toll Collection System Application (the "AutoPay+"). By using our App, you agree to comply with and be bound by the following terms and conditions of use. Please review these terms carefully. If you do not agree to these terms, you should not use this App.

  Acceptance of Terms
  By accessing or using the App in any manner, you agree to be bound by these terms and conditions, our Privacy Policy, and all other operating rules, policies, and procedures that may be published from time to time on the App, each of which is incorporated by reference into these terms.

  Use of the App
  You may use the App solely for your personal and non-commercial purposes. You agree that you will not use the App for any illegal or unauthorized purpose. You must not, in the use of the App, violate any laws in your jurisdiction.

  User Accounts
  In order to access certain features of the App, you may be required to create a user account. You are responsible for maintaining the confidentiality of your account and password and for restricting access to your device. You agree to accept responsibility for all activities that occur under your account.

  Bank Information and Wallet
  By providing your bank information and/or setting up a wallet within the App, you authorize us to securely process your payments for toll collections. You agree to provide accurate and complete information regarding your bank account and/or wallet and to promptly update such information as necessary.

  Notifications
  The App may send notifications to alert you of toll collections, account activity, promotions, or other relevant information. You may opt-out of receiving certain types of notifications through the settings in the App.

  Intellectual Property
  All content included in the App, including text, graphics, logos, images, audio clips, and software, is the property of our company or its licensors and is protected by copyright laws.

  Limitation of Liability
  Our company shall not be liable for any indirect, incidental, special, consequential, or punitive damages, or any loss of profits or revenues, whether incurred directly or indirectly, or any loss of data, use, goodwill, or other intangible losses resulting from (a) your access to or use of or inability to access or use the App; (b) any conduct or content of any third party on the App; or (c) unauthorized access, use, or alteration of your transmissions or content.

  Changes to Terms
  We reserve the right to modify or replace these terms and conditions at any time. It is your responsibility to review these terms periodically for changes. Your continued use of the App following the posting of any changes constitutes acceptance of those changes.

  Governing Law
  These terms shall be governed by and construed in accordance with the laws of [Your Country], without regard to its conflict of law provisions.

  Contact Us
  If you have any questions about these terms and conditions, please contact us at [Your Contact Information].

  By using this App, you signify your acceptance of these terms and conditions. If you do not agree to these terms, please do not use our App.


  ''';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: const EdgeInsets.only(top: 100, left: 30, right: 30, bottom: 50),
      width: 500,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFF622774), // Start color
            Color(0xFF0D324D), // End color
          ],
        ),
      ),
      child: Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: const [
                BoxShadow(
                  color: Colors.grey,
                  spreadRadius: 4,
                  blurRadius: 2,
                )
              ]),
          child: SingleChildScrollView(
            child: Container(
                child: Text(terms, style: const TextStyle(fontSize: 18))),
          )),
    ));
  }
}
