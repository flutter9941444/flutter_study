// ignore_for_file: deprecated_member_use

// Import your FirestoreService
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final TextEditingController _vehicleHolderNameController =
      TextEditingController();
  final TextEditingController _vehicleTypeController = TextEditingController();
  final TextEditingController _vehicleNameController = TextEditingController();
  final TextEditingController _vehicleNumberController =
      TextEditingController();

  final TextEditingController _bankNameController = TextEditingController();
  final TextEditingController _ifscCodeController = TextEditingController();
  final TextEditingController _cardExpiryDateController =
      TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign Up"),
        backgroundColor: Colors.deepPurple,
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Colors.deepPurple, Color.fromARGB(255, 148, 207, 255)],
          ),
        ),
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.8),
              borderRadius: BorderRadius.circular(16.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: const Offset(0, 3),
                ),
              ],
            ),
            padding: const EdgeInsets.all(6.0),
            width: 350,
            height: 750,
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "Personal Details: ",
                      style: TextStyle(fontSize: 22),
                    ),
                    const SizedBox(height: 4),
                    // Personal Info
                    _buildTextField("Full Name", _fullNameController),
                    const SizedBox(height: 4),
                    _buildTextField("Username", _usernameController),
                    const SizedBox(height: 4),
                    _buildTextField("Email", _emailController),
                    const SizedBox(height: 4),
                    _buildTextField("Mobile", _mobileController),
                    const SizedBox(height: 4),
                    // _buildTextField("Date of Birth", _dobController),
                    // const SizedBox(height: 4),
                    _buildTextField("Password", _passwordController,
                        isPassword: true),

                    const SizedBox(height: 18.0),
                    const Text(
                      "Vehicle Details : ",
                      style: TextStyle(fontSize: 22),
                    ),

                    // Vehicle Info
                    _buildTextField(
                        "Vehicle Holder Name", _vehicleHolderNameController),
                    const SizedBox(height: 4),
                    // _buildTextField("Vehicle Type", _vehicleTypeController),
                    // const SizedBox(height: 4),
                    _buildTextField("Vehicle Name", _vehicleNameController),
                    const SizedBox(height: 4),
                    _buildTextField("Vehicle Number", _vehicleNumberController),
                    const SizedBox(height: 4),

                    const SizedBox(height: 18.0),

                    // Bank Info
                    const Text(
                      "Bank Details : ",
                      style: TextStyle(fontSize: 22),
                    ),
                    _buildTextField("Bank Name", _bankNameController),
                    const SizedBox(height: 4),
                    _buildTextField("IFSC Code", _ifscCodeController),
                    const SizedBox(height: 4),
                    _buildTextField(
                        "Card Expiry Date", _cardExpiryDateController),

                    const SizedBox(height: 24.0),

                    _buildElevatedButton("Sign Up", _handleSignUp),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(String labelText, TextEditingController controller,
      {bool isPassword = false}) {
    return TextFormField(
      controller: controller,
      obscureText: isPassword,
      style: const TextStyle(color: Colors.deepPurple),
      decoration: InputDecoration(
        labelText: labelText,
        hintText: 'Enter your $labelText',
        border: const OutlineInputBorder(),
        filled: true,
        fillColor: Colors.grey[200],
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.deepPurple, width: 2.0),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter $labelText';
        }
        return null;
      },
    );
  }

  Widget _buildElevatedButton(String text, VoidCallback onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      /*style: ElevatedButton.styleFrom(
        primary: Colors.deepPurple,
        onPrimary: Colors.white,
        padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 12.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 5.0,
      ),*/
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.deepPurple),
      ),
      child: Text(
        text,
        style: const TextStyle(fontSize: 16.0),
      ),
    );
  }

  void _handleSignUp() {
    if (_formKey.currentState!.validate()) {
      void _handleSignUp() async {}
    }
  }
}
