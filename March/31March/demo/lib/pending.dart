import 'package:flutter/material.dart';

class Pending extends StatelessWidget {
  const Pending({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding:
            const EdgeInsets.only(top: 100, left: 30, right: 30, bottom: 50),
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF622774), // Start color
              Color(0xFF0D324D), // End color
            ],
          ),
        ),
        child: Center(
          child: Container(
            height: 60,
            width: 190,
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 4,
                    blurRadius: 2,
                  )
                ]),
            child: const Text("No Pending Work"),
          ),
        ),
      ),
    );
  }
}
