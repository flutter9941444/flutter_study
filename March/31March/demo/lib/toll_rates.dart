import 'package:flutter/material.dart';

class TollRates extends StatefulWidget {
  @override
  _TollRatesState createState() => _TollRatesState();
}

class _TollRatesState extends State<TollRates> {
  String? _selectedState;
  String? _selectedCity;

  List<String> states = [
    'Andhra Pradesh',
    'Arunachal Pradesh',
    'Assam',
    'Bihar',
    'Chhattisgarh',
    'Goa',
    'Gujarat',
    'Haryana',
    'Himachal Pradesh',
    'Jharkhand',
    'Karnataka',
    'Kerala',
    'Madhya Pradesh',
    'Maharashtra',
    'Manipur',
    'Meghalaya',
    'Mizoram',
    'Nagaland',
    'Odisha',
    'Punjab',
    'Rajasthan',
    'Sikkim',
    'Tamil Nadu',
    'Telangana',
    'Tripura',
    'Uttar Pradesh',
    'Uttarakhand',
    'West Bengal'
  ];
  // Sample states
  Map<String, List<String>> cities = {
    'Andhra Pradesh': [
      'Visakhapatnam',
      'Vijayawada',
      'Guntur',
      'Nellore',
      'Kurnool',
      'Rajahmundry',
      'Tirupati',
      'Kadapa',
      'Kakinada',
      'Anantapur',
      'Chittoor',
      'Amalapuram',
      'Tenali',
      'Proddatur',
      'Eluru'
    ],
    'Arunachal Pradesh': [
      'Itanagar',
      'Naharlagun',
      'Pasighat',
      'Roing',
      'Tezu',
      'Ziro',
      'Aalo',
      'Bomdila',
      'Changlang',
      'Daporijo',
      'Khonsa',
      'Seppa',
      'Tawang',
      'Yingkiong'
    ],
    'Assam': [
      'Guwahati',
      'Silchar',
      'Dibrugarh',
      'Jorhat',
      'Nagaon',
      'Tinsukia',
      'Bongaigaon',
      'Tezpur',
      'Goalpara',
      'Sibsagar',
      'Dhubri',
      'Tinsukia',
      'Nalbari',
      'Lakhimpur',
      'Karimganj'
    ],
    'Bihar': [
      'Patna',
      'Gaya',
      'Bhagalpur',
      'Muzaffarpur',
      'Purnia',
      'Darbhanga',
      'Arrah',
      'Begusarai',
      'Katihar',
      'Munger',
      'Chhapra',
      'Bettiah',
      'Saharsa',
      'Sasaram',
      'Hajipur'
    ],
    'Chhattisgarh': [
      'Raipur',
      'Bhilai',
      'Bilaspur',
      'Korba',
      'Raigarh',
      'Jagdalpur',
      'Ambikapur',
      'Dhamtari',
      'Durg',
      'Rajnandgaon',
      'Kawardha',
      'Mahasamund',
      'Janjgir',
      'Champa',
      'Kanker'
    ],
    'Goa': [
      'Panaji',
      'Vasco da Gama',
      'Margao',
      'Mapusa',
      'Ponda',
      'Bicholim',
      'Curchorem',
      'Sanguem',
      'Quepem',
      'Canacona',
      'Valpoi',
      'Marcela',
      'Cuncolim',
      'Sanquelim',
      'Cortalim'
    ],
    'Gujarat': [
      'Ahmedabad',
      'Surat',
      'Vadodara',
      'Rajkot',
      'Bhavnagar',
      'Jamnagar',
      'Junagadh',
      'Gandhinagar',
      'Anand',
      'Bharuch',
      'Porbandar',
      'Nadiad',
      'Navsari',
      'Valsad',
      'Morbi'
    ],
    'Haryana': [
      'Faridabad',
      'Gurgaon',
      'Panipat',
      'Ambala',
      'Yamunanagar',
      'Rohtak',
      'Hisar',
      'Karnal',
      'Sonipat',
      'Panchkula',
      'Bhiwani',
      'Sirsa',
      'Bahadurgarh',
      'Jind',
      'Thanesar'
    ],
    'Himachal Pradesh': [
      'Shimla',
      'Solan',
      'Dharamsala',
      'Palampur',
      'Hamirpur',
      'Mandi',
      'Kangra',
      'Una',
      'Nahan',
      'Bilaspur',
      'Chamba',
      'Kullu',
      'Manali',
      'Sirmaur',
      'Kinnaur'
    ],
    'Jharkhand': [
      'Ranchi',
      'Jamshedpur',
      'Dhanbad',
      'Bokaro',
      'Deoghar',
      'Phusro',
      'Hazaribagh',
      'Giridih',
      'Ramgarh',
      'Medininagar',
      'Chirkunda',
      'Jhumri Tilaiya',
      'Saunda',
      'Sahibganj',
      'Gumia'
    ],
    'Karnataka': [
      'Bangalore',
      'Mysore',
      'Hubli',
      'Mangalore',
      'Belgaum',
      'Gulbarga',
      'Davangere',
      'Bellary',
      'Vijapur',
      'Shimoga',
      'Tumkur',
      'Bidar',
      'Hospet',
      'Raichur',
      'Hassan'
    ],
    'Kerala': [
      'Thiruvananthapuram',
      'Kochi',
      'Kozhikode',
      'Kollam',
      'Thrissur',
      'Alappuzha',
      'Palakkad',
      'Kannur',
      'Kottayam',
      'Manjeri',
      'Kasaragod',
      'Malappuram',
      'Pathanamthitta',
      'Kottarakkara',
      'Perinthalmanna'
    ],
    'Madhya Pradesh': [
      'Indore',
      'Bhopal',
      'Jabalpur',
      'Gwalior',
      'Ujjain',
      'Sagar',
      'Dewas',
      'Satna',
      'Ratlam',
      'Rewa',
      'Murwara',
      'Singrauli',
      'Burhanpur',
      'Khandwa',
      'Bhind'
    ],
    'Maharashtra': [
      'Mumbai',
      'Pune',
      'Nagpur',
      'Thane',
      'Nashik',
      'Kalyan-Dombivali',
      'Vasai-Virar',
      'Aurangabad',
      'Solapur',
      'Bhiwandi',
      'Amravati',
      'Nanded',
      'Kolhapur',
      'Sangli',
      'Jalgaon'
    ],
    'Manipur': [
      'Imphal',
      'Thoubal',
      'Lilong',
      'Kakching',
      'Ukhrul',
      'Churachandpur',
      'Senapati',
      'Kangpokpi',
      'Jiribam',
      'Tamenglong',
      'Bishnupur',
      'Chandel',
      'Tengnoupal',
      'Mao',
      'Noney'
    ],
    'Meghalaya': [
      'Shillong',
      'Tura',
      'Nongstoin',
      'Jowai',
      'Williamnagar',
      'Baghmara',
      'Resubelpara',
      'Khliehriat',
      'Mairang',
      'Mawkyrwat',
      'Cherrapunji',
      'Amlarem',
      'Nongpoh',
      'Dawki',
      'Mawphlang'
    ],
    'Mizoram': [
      'Aizawl',
      'Lunglei',
      'Saiha',
      'Champhai',
      'Kolasib',
      'Serchhip',
      'Lawngtlai',
      'Mamit',
      'Khawzawl',
      'Hnahthial',
      'Biate',
      'Tlabung',
      'Thenzawl',
      'Sairang',
      'Tuipang'
    ],
    'Nagaland': [
      'Kohima',
      'Dimapur',
      'Mokokchung',
      'Tuensang',
      'Wokha',
      'Zunheboto',
      'Phek',
      'Chumukedima',
      'Mon',
      'Longleng',
      'Kiphire',
      'Zunheboto',
      'Tizit',
      'Satakha',
      'Pungro'
    ],
    'Odisha': [
      'Bhubaneswar',
      'Cuttack',
      'Rourkela',
      'Brahmapur',
      'Sambalpur',
      'Puri',
      'Balasore',
      'Bhadrak',
      'Baripada',
      'Jharsuguda',
      'Jeypore',
      'Balangir',
      'Kendujhar',
      'Rayagada',
      'Bargarh'
    ],
    'Punjab': [
      'Ludhiana',
      'Amritsar',
      'Jalandhar',
      'Patiala',
      'Bathinda',
      'Mohali',
      'Hoshiarpur',
      'Batala',
      'Pathankot',
      'Moga',
      'Abohar',
      'Malerkotla',
      'Khanna',
      'Phagwara',
      'Muktasar'
    ],
    'Rajasthan': [
      'Jaipur',
      'Jodhpur',
      'Kota',
      'Bikaner',
      'Ajmer',
      'Udaipur',
      'Bhilwara',
      'Alwar',
      'Bharatpur',
      'Sikar',
      'Pali',
      'Ganganagar',
      'Kishangarh',
      'Barmer',
      'Tonk'
    ],
    'Sikkim': [
      'Gangtok',
      'Namchi',
      'Mangan',
      'Gyalshing',
      'Singtam',
      'Rangpo',
      'Soreng',
      'Ravangla',
      'Jorethang',
      'Nayabazar',
      'Lachung',
      'Chungthang',
      'Majitar',
      'Sanga',
      'Kabi'
    ],
    'Tamil Nadu': [
      'Chennai',
      'Coimbatore',
      'Madurai',
      'Tiruchirappalli',
      'Tiruppur',
      'Salem',
      'Erode',
      'Tirunelveli',
      'Vellore',
      'Thoothukkudi',
      'Dindigul',
      'Thanjavur',
      'Ranipet',
      'Nagercoil',
      'Karur'
    ],
    'Telangana': [
      'Hyderabad',
      'Warangal',
      'Nizamabad',
      'Khammam',
      'Karimnagar',
      'Ramagundam',
      'Mahbubnagar',
      'Mancherial',
      'Adilabad',
      'Suryapet',
      'Jagtial',
      'Miryalaguda',
      'Nalgonda',
      'Kamareddy',
      'Siddipet'
    ],
    'Tripura': [
      'Agartala',
      'Udaipur',
      'Dharmanagar',
      'Kailasahar',
      'Ambassa',
      'Khowai',
      'Belonia',
      'Kamalpur',
      'Sabroom',
      'Sonamura',
      'Santirbazar',
      'Amarpur',
      'Jirania',
      'Teliamura',
      'Kanchanpur'
    ],
    'Uttar Pradesh': [
      'Lucknow',
      'Kanpur',
      'Ghaziabad',
      'Agra',
      'Varanasi',
      'Meerut',
      'Allahabad',
      'Bareilly',
      'Aligarh',
      'Moradabad',
      'Saharanpur',
      'Gorakhpur',
      'Faizabad',
      'Jhansi',
      'Muzaffarnagar'
    ],
    'Uttarakhand': [
      'Dehradun',
      'Haridwar',
      'Roorkee',
      'Haldwani',
      'Rudrapur',
      'Kashipur',
      'Rishikesh',
      'Ramnagar',
      'Manglaur',
      'Kichha',
      'Sitarganj',
      'Jaspur',
      'Bageshwar',
      'Pithoragarh',
      'Nainital'
    ],
    'West Bengal': [
      'Kolkata',
      'Asansol',
      'Siliguri',
      'Durgapur',
      'Bardhaman',
      'Malda',
      'Baharampur',
      'Habra',
      'Kharagpur',
      'Krishnanagar',
      'Raiganj',
      'Bally',
      'Hugli',
      'Shantipur',
      'Bankura'
    ],
  };
// Sample cities

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF622774), // Start color
              Color(0xFF0D324D), // End color
            ],
          ),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 100, left: 20, right: 20, bottom: 400),
          child: Container(
            padding: const EdgeInsets.all(20),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: [
                    const SizedBox(
                      width: 50,
                    ),
                    const Text(
                      'State',
                      style: TextStyle(fontSize: 25.0, color: Colors.black),
                    ),
                    const SizedBox(width: 20),
                    Container(
                      padding: const EdgeInsets.all(6),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.4),
                          borderRadius: BorderRadius.circular(10)),
                      child: DropdownButton<String>(
                        value: _selectedState,
                        onChanged: (String? newValue) {
                          setState(() {
                            _selectedState = newValue!;
                            _selectedCity =
                                null; // Reset city selection when state changes
                          });
                        },
                        items: states.map((String state) {
                          return DropdownMenuItem<String>(
                            value: state,
                            child: Text(
                              state,
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 15),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 40.0),
                Row(
                  children: [
                    const SizedBox(width: 50),
                    const Text(
                      'City:',
                      style: TextStyle(
                        fontSize: 25.0,
                      ),
                    ),
                    const SizedBox(width: 20),
                    DropdownButton<String>(
                      value: _selectedCity,
                      onChanged: _selectedState == null
                          ? null
                          : (String? newValue) {
                              setState(() {
                                _selectedCity = newValue!;
                              });
                            },
                      items: _selectedState == null
                          ? null
                          : cities[_selectedState!]!.map((String city) {
                              return DropdownMenuItem<String>(
                                value: city,
                                child: Text(
                                  city,
                                  style: const TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                              );
                            }).toList(),
                    ),
                  ],
                ),
                const SizedBox(height: 40),
                Container(
                  padding: const EdgeInsets.only(
                      bottom: 6, top: 6, left: 12, right: 12),
                  decoration: BoxDecoration(
                      color: Colors.deepPurpleAccent,
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.deepPurple,
                          blurRadius: 4,
                        )
                      ]),
                  child: GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return TollRatesPopup();
                        },
                      );
                    },
                    child: const Text("Check",
                        style: TextStyle(fontSize: 20, color: Colors.white)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TollRatesPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Toll Rates',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 16),
              Container(
                width: MediaQuery.of(context).size.width *
                    2, // Set width to accommodate all columns
                child: Table(
                  border: TableBorder.all(),
                  columnWidths: {
                    0: FlexColumnWidth(2),
                    1: FlexColumnWidth(1),
                    2: FlexColumnWidth(1),
                    3: FlexColumnWidth(1),
                    4: FlexColumnWidth(1),
                  },
                  children: [
                    TableRow(
                      children: [
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Vehicle Type',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Hourly',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Daily',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Monthly',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Yearly',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Car',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.50',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.100',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.500',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.1000',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                    TableRow(
                      children: [
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Truck',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.100',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.500',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.1000',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Rs.2000',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Add more rows for different vehicle types and rates as needed
                  ],
                ),
              ),
              SizedBox(height: 16),
              Align(
                alignment: Alignment.center,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Close'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
