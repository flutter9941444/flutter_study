import 'package:flutter/material.dart';

class HelpLine extends StatelessWidget {
  const HelpLine({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF622774), // Start color
              Color(0xFF0D324D), // End color
            ],
          ),
        ),
        child: Padding(
            padding: const EdgeInsets.only(
              top: 140,
              bottom: 140,
            ),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                      height: 100,
                      width: 300,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 4,
                              spreadRadius: 4)
                        ],
                      ),
                      child: const Text("Om Bhandarkar \n 7798469674",
                          style: TextStyle(fontSize: 20))),
                  Container(
                      height: 100,
                      width: 300,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 4,
                              spreadRadius: 4)
                        ],
                      ),
                      child: const Text("Sail Bhandekar \n 9119499627",
                          style: TextStyle(fontSize: 20))),
                  Container(
                      height: 100,
                      width: 300,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 4,
                              spreadRadius: 4)
                        ],
                      ),
                      child: const Text("Yoshita Bhole \n 9158486162",
                          style: TextStyle(fontSize: 20))),
                  Container(
                      height: 100,
                      width: 300,
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 4,
                              spreadRadius: 4)
                        ],
                      ),
                      child: const Text("Namrata Chaudhari \n 8983806942",
                          style: TextStyle(fontSize: 20))),
                ])),
      ),
    );
  }
}
