// ignore_for_file: deprecated_member_use

import 'package:carousel_slider/carousel_slider.dart';

import 'FAQ.dart';
import 'feedback.dart';
import 'package:flutter/material.dart';

import 'helpline.dart';
import 'notification.dart';
import 'payments.dart';
import 'pending.dart';
import 'profile.dart';
import 'terms.dart';
import 'toll_rates.dart';
import 'walletScreen.dart';

String uname = '';

class Home extends StatefulWidget {
  const Home({super.key});
  @override
  State createState() => _Home();
}

class _Home extends State {
  //variables
  TextEditingController vehicleNo = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            //color: Colors.deepPurple,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 2.4,
                    blurRadius: 5,
                    offset: Offset(0, 3),
                  )
                ]),
            height: 110,
            padding: const EdgeInsets.only(
              top: 50,
              bottom: 20,
              left: 20,
              right: 20,
            ),
            child: Row(children: [
              IconButton(
                icon: const Icon(Icons.menu, size: 30),
                onPressed: () {
                  _showMenu(context);
                },
              ),
              const SizedBox(width: 10),
              Text("Hello $uname",
                  style: const TextStyle(fontSize: 20, height: 1.5)),
              const Spacer(),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const Notifications(),
                        ),
                      );
                    },
                    icon: const Icon(Icons.notifications,
                        size: 30, color: Colors.deepPurple)),
                IconButton(
                    onPressed: () {
                      fetchInfo();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const Profile(),
                        ),
                      );
                    },
                    icon: const Icon(Icons.person_rounded,
                        size: 30, color: Colors.deepPurple))
              ]),
            ]),
          ),
          Expanded(
            child: Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.all(20),
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(
                        color: Colors.deepPurple.withOpacity(0.5),
                        spreadRadius: 2.4,
                        blurRadius: 5,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ]),
                    height: 250,
                    child: Expanded(
                      child: Stack(
                        children: [
                          CarouselSlider(
                            items: [
                              // List of images for the image slider
                              Image.asset(
                                "assets/img1.jpg",
                                fit: BoxFit.cover,
                              ),
                              Image.asset(
                                "assets/img2.jpg",
                                fit: BoxFit.cover,
                              ),
                              Image.asset(
                                "assets/img3.jpg",
                                fit: BoxFit.cover,
                              ),
                              Image.asset(
                                "assets/img4.jpg",
                                fit: BoxFit.cover,
                              ),
                            ],
                            options: CarouselOptions(
                              height: double.infinity,
                              aspectRatio: 16 / 9,
                              viewportFraction: 0.8,
                              initialPage: 0,
                              enableInfiniteScroll: true,
                              reverse: false,
                              autoPlay: true,
                              autoPlayInterval: const Duration(seconds: 3),
                              autoPlayAnimationDuration:
                                  const Duration(milliseconds: 1000),
                              autoPlayCurve: Curves.fastOutSlowIn,
                              enlargeCenterPage: true,
                              onPageChanged: (index, reason) {
                                // Handle page change
                              },
                              scrollDirection: Axis
                                  .horizontal, // Set scroll direction to horizontal
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Colors.transparent,
                                  Colors.black.withOpacity(0.7),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: 10,
                            bottom: 5,
                          ),
                          child: Column(children: [
                            SizedBox(
                              height: 70,
                              width: 180,
                              child: TextField(
                                  controller: vehicleNo,
                                  decoration: const InputDecoration(
                                      hintText: "Enter vehicle no",
                                      enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.black),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12))),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(12)),
                                        borderSide: BorderSide(
                                            color: Colors.deepPurple),
                                      ))),
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  if (vehicleNo.text == "") {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: const Text("Error !!"),
                                          content: const Text(
                                              "Enter Vehicle number first"),
                                          actions: [
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: const Text('Close'),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  } else {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: const Text("Vehicle Status"),
                                          content: const Text(
                                              "Authorized with no actions"),
                                          actions: [
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: const Text('Close'),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  }
                                },
                                style: const ButtonStyle(
                                    shadowColor: MaterialStatePropertyAll(
                                        Colors.deepPurple),
                                    fixedSize:
                                        MaterialStatePropertyAll(Size(150, 30)),
                                    backgroundColor: MaterialStatePropertyAll(
                                        Colors.deepPurple)),
                                child: const Text("Check Status",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 15)))
                          ]),
                        ),
                        Container(
                            margin: const EdgeInsets.all(10),
                            height: 150,
                            width: 400,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.deepPurple.withOpacity(0.5),
                                    spreadRadius: 2.4,
                                    blurRadius: 5,
                                    offset: const Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ]),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  GestureDetector(
                                    onTap: () async {
                                      await fetchWallet();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => WalletPage(),
                                        ),
                                      );
                                    },
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            "assets/wallet.png",
                                            height: 60,
                                            width: 60,
                                          ),
                                          const SizedBox(height: 5),
                                          const Text("Wallet"),
                                        ]),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      fetchTime();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => Payments(),
                                        ),
                                      );
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "assets/bank.png",
                                          height: 60,
                                          width: 60,
                                        ),
                                        const SizedBox(height: 5),
                                        const Text("Payments"),
                                      ],
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => const Pending(),
                                        ),
                                      );
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "assets/expired.png",
                                          height: 60,
                                          width: 60,
                                        ),
                                        const SizedBox(height: 5),
                                        const Text("Pending \nActions"),
                                      ],
                                    ),
                                  )
                                ])),
                        Container(
                            margin: const EdgeInsets.all(10),
                            height: 150,
                            width: 400,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.deepPurple.withOpacity(0.5),
                                    spreadRadius: 2.4,
                                    blurRadius: 5,
                                    offset: const Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ]),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => TollRates(),
                                          ),
                                        );
                                      },
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            "assets/toll.png",
                                            height: 50,
                                            width: 50,
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          const Text("Check Tolls"),
                                        ],
                                      )),
                                  GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const HelpLine()));
                                      },
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            "assets/3d-accept.png",
                                            height: 50,
                                            width: 50,
                                          ),
                                          const SizedBox(height: 5),
                                          const Text("Toll-free"),
                                          const Text("helpline"),
                                        ],
                                      )),
                                ])),
                        Container(
                          margin: const EdgeInsets.all(10),
                          height: 150,
                          width: 400,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.deepPurple.withOpacity(0.5),
                                  spreadRadius: 2.4,
                                  blurRadius: 5,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const FAQ()));
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "assets/FAQ.jpg",
                                          height: 80,
                                          width: 80,
                                        ),
                                        //const SizedBox(height: 2),
                                        const Text("FAQ"),
                                      ],
                                    )),
                                GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const GiveFeedback()));
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "assets/online-survey.png",
                                          height: 50,
                                          width: 50,
                                        ),
                                        const SizedBox(height: 5),
                                        const Text("Feedback"),
                                      ],
                                    )),
                                GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const Terms()));
                                    },
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "assets/terms-and-conditions.png",
                                          height: 50,
                                          width: 50,
                                        ),
                                        const SizedBox(height: 5),
                                        const Text("T&C")
                                      ],
                                    )),
                              ]),
                        ),
                      ],
                    ),
                  )
                ]),
          ),
        ],
      ),
    );
  }

  void _showMenu(BuildContext context) {
    final RenderBox button = context.findRenderObject() as RenderBox;
    final RenderBox overlay =
        Overlay.of(context).context.findRenderObject() as RenderBox;
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        button.localToGlobal(Offset.zero, ancestor: overlay),
        button.localToGlobal(button.size.bottomLeft(Offset.zero),
            ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );

    showMenu(
      context: context,
      position: position,
      items: [
        const PopupMenuItem(
          value: 'help',
          child: Text('Help'),
        ),
        const PopupMenuItem(
          value: 'logout',
          child: Text('Log Out'),
        ),
      ],
    ).then<void>((value) {
      if (value == 'help') {
        // Show Help dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Help'),
              content: const Text('This is the help message.'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(); // Close the dialog
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          },
        );
      } else if (value == 'logout') {
        // Handle Log Out option
        Navigator.pop(context);
      }
    });
  }
}
