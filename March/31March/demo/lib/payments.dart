import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

List<String> dateTime = [];
Future<void> fetchTime() async {
  final QuerySnapshot querySnapshot = await FirebaseFirestore.instance
      .collection('users')
      .where('username', isEqualTo: 'namrata')
      .limit(1)
      .get();

  if (querySnapshot.docs.isNotEmpty) {
    final userData = querySnapshot.docs.first.data() as Map<String, dynamic>;
    if (userData.containsKey('time_date')) {
      List<dynamic> dts = userData['time_date'];

      // Clear the existing elements in the dateTime list
      dateTime.clear();

      // Iterate through each element in the 'time_date' array
      for (var dt in dts) {
        // Add the element to the dateTime list
        dateTime.add(dt.toString());
      }
    }
  }
}

class Payments extends StatelessWidget {
  Payments({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Toll History'),
      ),
      body: TollDetailsScreen(),
    );
  }
}

class TollDetailsScreen extends StatelessWidget {
  const TollDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFF622774), // Start color
            Color(0xFF0D324D), // End color
          ],
        ),
      ),
      child: ListView.builder(
        itemCount: dateTime.length,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: ListTile(
              title: Text('Toll collected on Narhe from Wallet',
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.w700)),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Amount: 50 points',
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.w500)),
                  Text('Payment Method: Wallet',
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.w500)),
                  Text('Date & Time: ${dateTime[index]}',
                      style: const TextStyle(
                          fontSize: 15, fontWeight: FontWeight.w500)),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
