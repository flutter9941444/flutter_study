import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'integration.dart';
import 'login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: 'AIzaSyAkpty-cd1Wfpnc544Gn4m7082nckF5m0I',
      appId: '1:976067318847:android:0cd6c812db5af2d42c548a',
      messagingSenderId: '976067318847',
      projectId: 'autopay-f2283',
    ),
  );
  await fetchData(); // Assuming fetchData is an asynchronous function
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login Page',
      home: LoginPage(),
    );
  }
}
