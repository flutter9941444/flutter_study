import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

int balance = 1000;
Future<void> fetchWallet() async {
  // You can directly use Firestore without initializing again
  final QuerySnapshot querySnapshot = await FirebaseFirestore.instance
      .collection('users')
      .where('username', isEqualTo: 'namrata')
      .limit(1)
      .get();

  if (querySnapshot.docs.isNotEmpty) {
    final userData = querySnapshot.docs.first.data() as Map<String, dynamic>;
    int totalTolls = userData['total_toll'];
    print("***************************");
    print('Total Tolls for Namrata: $totalTolls');
    print("***************************");
    int result = (1000 - totalTolls);
    balance = result;
  } else {
    print('No data found for the username Namrata.');
  }
}

class WalletPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Wallet'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/wal-removebg-preview.png"),
              ],
            ),
            Text(
              'Received Balance',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.blue[100],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Balance:',
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    '1000 points', // Replace with actual balance
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              'Available Balance',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.green[100],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Available:',
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    '$balance points', // Replace with actual rewards
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              'Recharge Wallet',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text("Recharge Wallet",
                            style: TextStyle(fontWeight: FontWeight.w500)),
                        content: const SingleChildScrollView(
                          child: Text("Connect Your Bank First",
                              style: TextStyle(fontSize: 18)),
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('Close'),
                          ),
                        ],
                      );
                    });
              },
              child: Text('Recharge Wallet'),
            ),
            SizedBox(height: 20),
            Text(
              'Help',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            ListTile(
                title: Text(
                  'How does the wallet work?',
                  style: TextStyle(
                    fontSize: 18, // Adjust the font size as needed
                    fontWeight: FontWeight.bold, // Making the text bold
                    //color: Colors.blue, // Changing the text color to blue
                  ),
                ),
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('How the Wallet Works',
                            style: TextStyle(fontWeight: FontWeight.w500)),
                        content: const SingleChildScrollView(
                          child: Text(
                            'Your wallet starts with 1000 points. Each time you pass a toll system of Autopay, your vehicle number is scanned and 50 points are deducted from your wallet. You can use these points to pay for tolls.',
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('Close'),
                          ),
                        ],
                      );
                    },
                  );
                }),
          ],
        ),
      ),
    );
  }
}
