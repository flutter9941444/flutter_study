import 'package:flutter/material.dart';

class Assign1 extends StatelessWidget {
  const Assign1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
        boxShadow: [
          BoxShadow(color: Colors.red, spreadRadius: 4, blurRadius: 4),
        ],
      ),
      child: ElevatedButton(onPressed: () {}, child: const Text("button")),
    )));
  }
}
