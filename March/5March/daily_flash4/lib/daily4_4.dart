import 'package:flutter/material.dart';

class Assign4 extends StatefulWidget {
  const Assign4({super.key});
  @override
  State createState() => _Assign4();
}

class _Assign4 extends State {
  bool temp = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
      onPressed: () {
        setState(() {
          temp = !temp;
        });
      },
      backgroundColor: (temp) ? Colors.orange : Colors.pinkAccent,
    ));
  }
}
