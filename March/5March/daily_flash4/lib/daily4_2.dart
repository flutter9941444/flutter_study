import 'package:flutter/material.dart';

class Assign2 extends StatelessWidget {
  const Assign2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      height: 200,
      width: 200,
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(color: Colors.red, spreadRadius: 4, blurRadius: 4),
        ],
      ),
      child: ElevatedButton(onPressed: () {}, child: const Text("button")),
    )));
  }
}
