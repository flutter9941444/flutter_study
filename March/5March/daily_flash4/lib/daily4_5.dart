import 'package:flutter/material.dart';

class Assign5 extends StatefulWidget {
  const Assign5({super.key});
  @override
  State createState() => _Assign5();
}

class _Assign5 extends State {
  bool temp = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              temp = !temp;
            });
          },
          backgroundColor: (temp) ? Colors.orange : Colors.pinkAccent,
        ));
  }
}
