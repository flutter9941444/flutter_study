import 'package:flutter/material.dart';

class Assign4 extends StatefulWidget {
  const Assign4({super.key});
  @override
  State createState() => _Assign4();
}

class _Assign4 extends State {
  Color color = Colors.red;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      height: 200,
      width: 300,
      decoration: BoxDecoration(
        color: Colors.pink,
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 4,
            offset: Offset(0, -4),
          )
        ],
        border: Border.all(color: color, width: 2),
      ),
      padding: const EdgeInsets.all(10),
    )));
  }
}
