import 'package:flutter/material.dart';

class Assign5 extends StatelessWidget {
  const Assign5({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
          width: 400,
          height: 400,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                stops: [0.5, 0.5], colors: [Colors.red, Colors.blue]),
            shape: BoxShape.circle,
            border: Border.all(width: 2),
          )),
    ));
  }
}
