import 'package:flutter/material.dart';

class Assign3 extends StatefulWidget {
  const Assign3({super.key});
  @override
  State createState() => _Assign3();
}

class _Assign3 extends State {
  Color color = Colors.red;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: GestureDetector(
      onTap: () {
        setState(
          () {
            if (color == Colors.red) {
              color = Colors.green;
            } else {
              color = Colors.red;
            }
          },
        );
      },
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
          border: Border.all(color: color, width: 4),
        ),
        padding: const EdgeInsets.all(10),
      ),
    )));
  }
}
