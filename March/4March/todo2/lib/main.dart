import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;

dynamic database;
List<TodoModelClass> taskList = [];

class TodoModelClass {
  String title;
  String description;
  String date;
  TodoModelClass({
    required this.title,
    required this.description,
    required this.date,
  });

  Map<String, dynamic> toDoMap() {
    return {'title': title, 'description': description, 'date': date};
  }
}

Future<void> main() async {
  WidgetsFlutterBinding
      .ensureInitialized(); // Ensure that Flutter bindings are initialized
  await openDB(); // Wait for the database to be opened
  runApp(const MainApp());
}

Future<void> openDB() async {
  database = await openDatabase(
    p.join(await getDatabasesPath(), "EmployeeDB.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''
        CREATE TABLE TodoModelClass(
          title TEXT PRIMARY KEY,
          description TEXT,
          date TEXT
        )
      ''');
    },
  );
  taskList = await getTask(); // Populate taskList after opening the database
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDo(),
      debugShowCheckedModeBanner: false,
    );
  }
}

Future<void> insertTask(TodoModelClass obj) async {
  final localDB = await database;
  await localDB.insert(
    "TodoModelClass",
    obj.toDoMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<TodoModelClass>> getTask() async {
  final localDB = await database;
  List<Map<String, dynamic>> taskList = await localDB.query("TodoModelClass");
  return taskList.map((taskMap) {
    return TodoModelClass(
      title: taskMap['title'],
      description: taskMap['description'],
      date: taskMap['date'],
    );
  }).toList();
}

Future<void> deleteTask(String title) async {
  final localDB = await database;
  await localDB.delete(
    'TodoModelClass',
    where: 'title = ?',
    whereArgs: [title],
  );
}

Future<void> updateTask(TodoModelClass updatedTask) async {
  final localDB = await database;
  await localDB.update(
    'TodoModelClass',
    updatedTask.toDoMap(),
    where: 'title = ?',
    whereArgs: [updatedTask.title],
  );
}

class ToDo extends StatefulWidget {
  const ToDo({Key? key});

  @override
  State createState() => _ToDo();
}

class _ToDo extends State<ToDo> {
  Color primaryColor = Colors.deepPurple;

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descripController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  bool isEdit = false;
  String msg = "";
  Color defaultColor = Colors.deepPurple;
  void createSheet(TodoModelClass obj) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      context: context,
      builder: ((context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 20),
              Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w600,
                  fontSize: 25,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Title : ",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(height: 4),
                    TextField(
                      controller: _titleController,
                      decoration: InputDecoration(
                        hintText: "  Enter Title",
                        border: InputBorder.none,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(color: defaultColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: defaultColor,
                          ),
                        ),
                      ),
                      cursorColor: defaultColor,
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "Description : ",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(height: 4),
                    TextField(
                      controller: _descripController,
                      decoration: InputDecoration(
                        hintText: "  Enter Description",
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(color: defaultColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: defaultColor,
                          ),
                        ),
                      ),
                      cursorColor: defaultColor,
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "Date: ",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(height: 4),
                    TextField(
                      onTap: () async {
                        DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2024),
                          lastDate: DateTime(2025),
                        );
                        if (pickedDate != null) {
                          String formatDate =
                              DateFormat.yMMMd().format(pickedDate);
                          setState(() {
                            _dateController.text = formatDate;
                          });
                        }
                      },
                      controller: _dateController,
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.calendar_month_outlined,
                          color: defaultColor,
                        ),
                        hintText: "  Select Date",
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(color: defaultColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: defaultColor,
                          ),
                        ),
                      ),
                      cursorColor: defaultColor,
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(defaultColor),
                          ),
                          onPressed: () async {
                            if (_titleController.text == "" ||
                                _descripController.text == "" ||
                                _dateController.text == "") {
                              setState(() {
                                msg = "Please fill all fields";
                              });
                            } else {
                              final newTask = TodoModelClass(
                                title: _titleController.text,
                                description: _descripController.text,
                                date: _dateController.text,
                              );
                              await insertTask(newTask);
                              setState(() {
                                taskList.add(newTask);
                              });
                              Navigator.pop(context);
                            }
                          },
                          child: Text(
                            "Save",
                            style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w400,
                              fontSize: 25,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Text(msg, style: const TextStyle(color: Colors.red)),
                  ],
                ),
              )
            ],
          ),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _titleController.text = "";
            _descripController.text = "";
            _dateController.text = "";
          });
          isEdit = false;
          createSheet(TodoModelClass(title: "", description: "", date: ""));
        },
        backgroundColor: defaultColor,
        child: const Icon(Icons.add),
      ),
      backgroundColor: primaryColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 40, left: 20, bottom: 30),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Good Morning", style: TextStyle(fontSize: 30)),
                Text("Namrata",
                    style: TextStyle(
                        fontSize: 30,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w500)),
              ],
            ),
          ),
          Expanded(
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                color: Colors.grey.shade400,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(
                      bottom: 20,
                    ),
                    child: Text("CREATE TO DO LIST",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500)),
                  ),
                  Expanded(
                    child: Container(
                      height: 685,
                      width: 440,
                      padding: const EdgeInsets.only(top: 10),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25),
                        ),
                      ),
                      child: ListView.builder(
                        itemCount: taskList.length,
                        itemBuilder: (context, index) {
                          return Slidable(
                            closeOnScroll: true,
                            endActionPane: ActionPane(
                              extentRatio: 0.2,
                              motion: const DrawerMotion(),
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      const SizedBox(height: 5),
                                      GestureDetector(
                                        onTap: () {
                                          _titleController.text =
                                              taskList[index].title;
                                          _descripController.text =
                                              taskList[index].description;
                                          _dateController.text =
                                              taskList[index].date;
                                          isEdit = true;
                                          createSheet(taskList[index]);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.all(10),
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            color: const Color.fromRGBO(
                                                89, 57, 241, 1),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: const Icon(
                                            Icons.edit,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      GestureDetector(
                                        onTap: () async {
                                          setState(() async {
                                            await deleteTask(
                                                taskList[index].title);
                                          });
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.all(5),
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            color: const Color.fromRGBO(
                                                89, 57, 241, 1),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: const Icon(
                                            Icons.delete,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 5),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            key: ValueKey(index),
                            child: Container(
                              margin: const EdgeInsets.all(6),
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.shade200,
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                  ),
                                ],
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(20),
                                    child: Image.asset("assets/documents.png",
                                        height: 50, width: 50),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      SizedBox(
                                        width: 270,
                                        child: Text(
                                          taskList[index].title,
                                          style: const TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                      const SizedBox(height: 2),
                                      SizedBox(
                                        width: 270,
                                        child: Text(
                                          taskList[index].description,
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                      ),
                                      const SizedBox(height: 6),
                                      SizedBox(
                                        width: 270,
                                        child: Text(
                                          taskList[index].date,
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Checkbox(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    activeColor: Colors.green,
                                    value:
                                        true, // Need to update this to reflect actual completion status
                                    onChanged: (val) {},
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
