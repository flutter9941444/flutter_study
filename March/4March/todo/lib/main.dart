import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDo(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ToDo extends StatefulWidget {
  const ToDo({super.key});
  @override
  State createState() => _ToDo();
}

class _ToDo extends State {
  Color primaryColor = Colors.deepPurple;
  List taskLst = [
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
    {
      'title': 'Lorem Ipsum is simply dummy industry. ',
      'description':
          'Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ',
      'date': '10 July 2023'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: primaryColor,
          child: const Icon(Icons.add),
          onPressed: () {},
        ),
        backgroundColor: primaryColor,
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              padding: const EdgeInsets.only(top: 40, left: 20, bottom: 30),
              child: const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Good Morning",
                      style: TextStyle(
                        fontSize: 30,
                      )),
                  Text("Namrata",
                      style: TextStyle(
                          fontSize: 30,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w500)),
                ],
              )),
          Expanded(
            child: Container(
                width: double.infinity,
                padding: const EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))),
                child: Column(children: [
                  const Padding(
                    padding: EdgeInsets.only(
                      bottom: 20,
                    ),
                    child: Text("CREATE TO DO LIST",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500)),
                  ),
                  Container(
                    height: 685,
                    width: 440,
                    padding: const EdgeInsets.only(top: 10),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)),
                    ),
                    child: ListView.builder(
                      itemCount: taskLst.length,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: const EdgeInsets.all(6),
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.shade200,
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                )
                              ]),
                          child: Row(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(20),
                                child: Image.asset("assets/documents.png",
                                    height: 50, width: 50),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                      width: 270,
                                      child: Text(
                                        taskLst[index]['title'],
                                        style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600),
                                      )),
                                  const SizedBox(height: 2),
                                  SizedBox(
                                    width: 270,
                                    child: Text(taskLst[index]['description'],
                                        style: const TextStyle(fontSize: 20)),
                                  ),
                                  const SizedBox(height: 6),
                                  SizedBox(
                                    width: 270,
                                    child: Text(taskLst[index]['date'],
                                        style: const TextStyle(fontSize: 20)),
                                  )
                                ],
                              ),
                              const Icon(
                                Icons.check_box_outline_blank_rounded,
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ])),
          )
        ]));
  }
}
