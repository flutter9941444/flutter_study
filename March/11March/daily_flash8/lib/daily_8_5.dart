import 'package:flutter/material.dart';

class Assign5 extends StatelessWidget {
  const Assign5({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Text("Daily Flash"),
      ),
      body: ListView(children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(),
          ),
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              const SizedBox(
                width: 300,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Title 1",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600)),
                      SizedBox(height: 4),
                      Text("give here some description",
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ]),
              ),
              Container(
                height: 40,
                width: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepPurple,
                ),
                child: const Icon(Icons.add, color: Colors.white),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
