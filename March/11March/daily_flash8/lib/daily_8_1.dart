import 'package:flutter/material.dart';

class Assign1 extends StatelessWidget {
  const Assign1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(border: Border.all()),
          width: 350,
          height: 550,
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Row(children: [
              Container(
                color: Colors.yellow,
                height: 200,
                width: 100,
              ),
              const Spacer(),
              Container(
                color: Colors.red,
                height: 200,
                width: 100,
              ),
            ]),
            Row(children: [
              Container(
                height: 100,
                width: 300,
                color: Colors.green,
                margin: const EdgeInsets.all(10),
              ),
            ]),
            Row(children: [
              Container(
                height: 200,
                width: 100,
                color: Colors.deepPurpleAccent,
              ),
              const Spacer(),
              Container(
                height: 200,
                width: 100,
                color: Colors.blueAccent,
              ),
            ]),
          ]),
        ),
      ),
    );
  }
}
