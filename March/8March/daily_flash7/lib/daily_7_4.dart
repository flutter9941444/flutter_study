import 'package:flutter/material.dart';

class Assign4 extends StatelessWidget {
  const Assign4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Row(
        children: [
          Expanded(
            flex: 6,
            child: Container(
              height: 100,
              color: Colors.red,
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              height: 100,
              color: Colors.yellow,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              height: 100,
              color: Colors.pink,
            ),
          ),
        ],
      ),
    ));
  }
}
