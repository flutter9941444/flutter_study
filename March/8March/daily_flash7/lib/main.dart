import 'package:daily_flash7/daily_7_4.dart';
import 'package:flutter/material.dart';

import 'daily_7_2.dart';
import 'daily_7_3.dart';
import 'daily_7_5.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assign5(),
    );
  }
}
