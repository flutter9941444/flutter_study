import 'package:flutter/material.dart';

class Assign2 extends StatelessWidget {
  const Assign2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(),
        ),
        child: const Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.star, size: 40),
            SizedBox(width: 5),
            Text("Ratings")
          ],
        ),
      ),
    ));
  }
}
