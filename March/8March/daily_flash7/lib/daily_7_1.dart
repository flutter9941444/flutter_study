import 'package:flutter/material.dart';

class Assign1 extends StatelessWidget {
  const Assign1({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Row(children: [
        Container(
          color: Colors.red,
          height: 100,
          width: 100,
        ),
        Container(
          color: Colors.yellow,
          height: 80,
          width: 80,
        ),
        Container(
          color: Colors.pink,
          height: 70,
          width: 80,
        ),
      ]),
    ));
  }
}
