import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: EduApp(),
    );
  }
}

class EduApp extends StatefulWidget {
  const EduApp({super.key});
  @override
  State createState() => _EduApp();
}

class _EduApp extends State {
  TextEditingController keyword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.lightBlue.shade200,
        padding: const EdgeInsets.all(20),
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              children: [
                Icon(Icons.menu),
                Spacer(),
                Icon(Icons.notifications),
              ],
            ),
            const SizedBox(height: 10),
            const Text("Welcome to New",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300)),
            const Text("Educourse",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600)),
            const SizedBox(height: 10),
            TextField(
              controller: keyword,
              decoration: const InputDecoration(
                  hintText: "Enter your keywords",
                  filled: true,
                  fillColor: Colors.white,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  )),
            ),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              padding: const EdgeInsets.only(
                top: 20,
                left: 10,
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Course For You",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500)),
                    Row(
                      children: [
                        Container(
                            height: 242,
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [Colors.pink, Colors.deepPurple]),
                            ),
                            child: Image.network(
                              "https://i.pinimg.com/564x/e7/df/63/e7df6315086a59795492e489f14d9237.jpg",
                              width: 100,
                              height: 100,
                            )),
                        Container(
                            height: 242,
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [Colors.pink, Colors.deepPurple]),
                            ),
                            child: Image.network(
                              "https://i.pinimg.com/564x/e7/df/63/e7df6315086a59795492e489f14d9237.jpg",
                              height: 100,
                              width: 100,
                            ))
                      ],
                    ),
                    const SizedBox(height: 20),
                    const Text("Courses by category",
                        style: TextStyle(fontSize: 20)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            Image.network(
                              "https://cdn-icons-png.flaticon.com/128/13191/13191281.png",
                              width: 50,
                              height: 50,
                            ),
                            const Text("UI/UX")
                          ],
                        ),
                        Column(
                          children: [
                            Image.network(
                                "https://t3.ftcdn.net/jpg/04/62/69/02/240_F_462690251_bIe2DWvA5oMyK8XvmktphBKRmz9owR6V.jpg",
                                width: 50,
                                height: 50),
                            const Text("VISUAL"),
                          ],
                        ),
                        Column(
                          children: [
                            Image.network(
                                "https://cdn-icons-png.flaticon.com/128/11772/11772951.png",
                                width: 50,
                                height: 50),
                            const Text("ILLUSTRATION")
                          ],
                        ),
                        Column(
                          children: [
                            Image.network(
                                "https://cdn-icons-png.flaticon.com/128/4152/4152019.png",
                                width: 50,
                                height: 50),
                            const Text("PHOTO")
                          ],
                        ),
                      ],
                    ),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
