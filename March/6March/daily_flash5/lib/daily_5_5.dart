import 'package:flutter/material.dart';

class Assign5 extends StatelessWidget {
  const Assign5({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Image.network(
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDdOOTA8p1E7Nt9haR0hzIsNKjhWSkYBvfBQ&usqp=CAU",
            height: 100,
            width: 100,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.red,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.blue,
          ),
        ]),
      ),
    );
  }
}
