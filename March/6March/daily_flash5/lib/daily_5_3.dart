import 'package:flutter/material.dart';

class Assign3 extends StatelessWidget {
  const Assign3({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Image.network(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3HdpIa27n4KHGcPb_Oav4pRK_TfkfnNVd5w&usqp=CAU"),
          Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(width: 2),
                boxShadow: const [
                  BoxShadow(blurRadius: 2, spreadRadius: 4, color: Colors.grey)
                ],
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            margin: const EdgeInsets.all(20),
            child: const Text("Namrata", style: TextStyle(fontSize: 16)),
          ),
        ]),
      ),
    );
  }
}
