import 'package:flutter/material.dart';

class Assign2 extends StatelessWidget {
  const Assign2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
              margin: const EdgeInsets.only(top: 8),
              height: 100,
              width: 200,
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7jBzHFuwudZsYIz9yKFMvG8GpoxDAbPzydA&usqp=CAU",
                  height: 100,
                  width: 100)),
          Container(
              height: 100,
              width: 200,
              margin: const EdgeInsets.only(top: 8),
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7jBzHFuwudZsYIz9yKFMvG8GpoxDAbPzydA&usqp=CAU",
                  width: 100,
                  height: 100)),
          Container(
              height: 100,
              width: 200,
              margin: const EdgeInsets.only(top: 8),
              child: Image.network(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7jBzHFuwudZsYIz9yKFMvG8GpoxDAbPzydA&usqp=CAU",
                  width: 100,
                  height: 100))
        ]),
      ),
    );
  }
}
