import 'package:flutter/material.dart';

class Assign4 extends StatelessWidget {
  const Assign4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Container(
            height: 100,
            width: 100,
            color: Colors.pink,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.deepPurple,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.greenAccent,
          ),
        ]),
      ),
    );
  }
}
