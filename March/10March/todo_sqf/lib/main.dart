// ignore_for_file: use_build_context_synchronously, non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;

dynamic database;

class TodoModelClass {
  int? taskId;
  String title;
  String description;
  String date;
  TodoModelClass({
    this.taskId,
    required this.title,
    required this.description,
    required this.date,
  });

  @override
  String toString() {
    return "{$taskId , $title , $description , $date}";
  }

  Map<String, dynamic> toDoMap() {
    return {'title': title, 'description': description, 'date': date};
  }
}

List<TodoModelClass> taskList = [];
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await openDB();
  taskList = await getTask();
  runApp(const MainApp());
}

Future<void> openDB() async {
  database = await openDatabase(
    p.join(await getDatabasesPath(), "TodoModelDB.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''
        CREATE TABLE Todo(
          taskId INTEGER PRIMARY KEY AUTOINCREMENT,
          title TEXT ,
          description TEXT,
          date TEXT
        )
      ''');
    },
  );
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDo(),
      debugShowCheckedModeBanner: false,
    );
  }
}

Future<void> insertTask(TodoModelClass obj) async {
  final localDB = await database;
  await localDB.insert(
    "Todo",
    obj.toDoMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<TodoModelClass>> getTask() async {
  final localDB = await database;
  List<Map<String, dynamic>> taskList = await localDB.query("Todo");
  return List.generate(taskList.length, (i) {
    return TodoModelClass(
        taskId: taskList[i]['taskId'],
        title: taskList[i]['title'],
        description: taskList[i]['description'],
        date: taskList[i]['date']);
  });
}

Future<void> deleteTask(TodoModelClass obj) async {
  //print("**** ${obj.taskId}");
  //print(await getTask());
  final localDB = await database;
  await localDB.delete(
    'Todo',
    where: 'taskId = ?',
    whereArgs: [obj.taskId],
  );
  // print("***");
  // print(await getTask());
  taskList = await getTask();
  // print(taskList);
}

Future<void> updateTask(TodoModelClass updatedTask) async {
  // print("---------------");
  // print(await getTask());
  final localDB = await database;
  await localDB.update(
    'Todo',
    updatedTask.toDoMap(),
    where: 'taskId = ?',
    whereArgs: [updatedTask.taskId],
  );
  // print(await getTask());
  taskList = await getTask();
}

class ToDo extends StatefulWidget {
  const ToDo({super.key});

  @override
  State createState() => _ToDo();
}

class _ToDo extends State<ToDo> {
  bool hidePwd = true;
  Color primaryColor = Colors.deepPurple;
  bool isValidate = false;
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descripController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  bool isEdit = false;
  String msg = "";
  //controllers
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  Color defaultColor = Colors.deepPurple;
  List validation = [
    {'name': 'namrata', 'pwd': 'Namrata@123'},
    {'name': 'chaudhari', 'pwd': 'namu'}
  ];

  void createSheet(TodoModelClass obj) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      context: context,
      builder: ((context) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 20),
              Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w600,
                  fontSize: 25,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Title : ",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(height: 4),
                    TextField(
                      controller: _titleController,
                      decoration: InputDecoration(
                        hintText: "  Enter Title",
                        border: InputBorder.none,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(color: defaultColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: defaultColor,
                          ),
                        ),
                      ),
                      cursorColor: defaultColor,
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "Description : ",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(height: 4),
                    TextField(
                      controller: _descripController,
                      decoration: InputDecoration(
                        hintText: "  Enter Description",
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(color: defaultColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: defaultColor,
                          ),
                        ),
                      ),
                      cursorColor: defaultColor,
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "Date: ",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w400,
                        fontSize: 20,
                      ),
                    ),
                    const SizedBox(height: 4),
                    TextField(
                      onTap: () async {
                        DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2024),
                          lastDate: DateTime(2025),
                        );
                        if (pickedDate != null) {
                          String formatDate =
                              DateFormat.yMMMd().format(pickedDate);
                          setState(() {
                            _dateController.text = formatDate;
                          });
                        }
                      },
                      controller: _dateController,
                      decoration: InputDecoration(
                        suffixIcon: Icon(
                          Icons.calendar_month_outlined,
                          color: defaultColor,
                        ),
                        hintText: "  Select Date",
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(color: defaultColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: defaultColor,
                          ),
                        ),
                      ),
                      cursorColor: defaultColor,
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(defaultColor),
                          ),
                          onPressed: () async {
                            if (_titleController.text == "" ||
                                _descripController.text == "" ||
                                _dateController.text == "") {
                              setState(() {
                                msg = "Please fill all fields";
                              });
                            } else {
                              final newTask = TodoModelClass(
                                taskId: obj.taskId,
                                title: _titleController.text,
                                description: _descripController.text,
                                date: _dateController.text,
                              );
                              if (isEdit == true) {
                                await updateTask(newTask);
                              } else {
                                await insertTask(newTask);
                              }
                              taskList = await getTask();
                              setState(() {});
                              Navigator.pop(context);
                            }
                          },
                          child: Text(
                            "Save",
                            style: GoogleFonts.quicksand(
                              fontWeight: FontWeight.w400,
                              fontSize: 25,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Text(msg, style: const TextStyle(color: Colors.red)),
                  ],
                ),
              )
            ],
          ),
        );
      }),
    );
  }

  Scaffold Page() {
    if (isValidate) {
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _titleController.text = "";
              _descripController.text = "";
              _dateController.text = "";
            });
            isEdit = false;
            createSheet(TodoModelClass(title: "", description: "", date: ""));
          },
          backgroundColor: defaultColor,
          child: const Icon(Icons.add),
        ),
        backgroundColor: primaryColor,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(top: 40, left: 20, bottom: 30),
              child: const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Good Morning", style: TextStyle(fontSize: 30)),
                  Text("Namrata",
                      style: TextStyle(
                          fontSize: 30,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w500)),
                ],
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  color: Colors.grey.shade400,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  ),
                ),
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        bottom: 20,
                      ),
                      child: Text("CREATE TO DO LIST",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500)),
                    ),
                    Expanded(
                      child: Container(
                        height: 685,
                        width: 440,
                        padding: const EdgeInsets.only(top: 10),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25),
                          ),
                        ),
                        child: ListView.builder(
                          itemCount: taskList.length,
                          itemBuilder: (context, index) {
                            return Slidable(
                              closeOnScroll: true,
                              endActionPane: ActionPane(
                                extentRatio: 0.2,
                                motion: const DrawerMotion(),
                                children: [
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        const SizedBox(height: 5),
                                        GestureDetector(
                                          onTap: () {
                                            _titleController.text =
                                                taskList[index].title;
                                            _descripController.text =
                                                taskList[index].description;
                                            _dateController.text =
                                                taskList[index].date;

                                            isEdit = true;
                                            createSheet(taskList[index]);
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(10),
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              color: const Color.fromRGBO(
                                                  89, 57, 241, 1),
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            child: const Icon(
                                              Icons.edit,
                                              color: Colors.white,
                                              size: 20,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(height: 20),
                                        GestureDetector(
                                          onTap: () async {
                                            TodoModelClass obj = TodoModelClass(
                                                taskId: taskList[index].taskId,
                                                title: taskList[index].title,
                                                description:
                                                    taskList[index].description,
                                                date: taskList[index].date);
                                            await deleteTask(obj);
                                            // taskList = await getTask();
                                            setState(
                                              () {},
                                            );
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(5),
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              color: const Color.fromRGBO(
                                                  89, 57, 241, 1),
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            child: const Icon(
                                              Icons.delete,
                                              color: Colors.white,
                                              size: 20,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(height: 5),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              key: ValueKey(index),
                              child: Container(
                                margin: const EdgeInsets.all(6),
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.shade200,
                                      spreadRadius: 2,
                                      blurRadius: 2,
                                    ),
                                  ],
                                ),
                                child: Row(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(20),
                                      child: Image.asset("assets/documents.png",
                                          height: 50, width: 50),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        SizedBox(
                                          width: 200,
                                          child: Text(
                                            taskList[index].title,
                                            style: const TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                        const SizedBox(height: 2),
                                        SizedBox(
                                          width: 200,
                                          child: Text(
                                            taskList[index].description,
                                            style:
                                                const TextStyle(fontSize: 20),
                                          ),
                                        ),
                                        const SizedBox(height: 6),
                                        SizedBox(
                                          width: 240,
                                          child: Text(
                                            taskList[index].date,
                                            style:
                                                const TextStyle(fontSize: 20),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Checkbox(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      activeColor: Colors.green,
                                      value:
                                          true, // Need to update this to reflect actual completion status
                                      onChanged: (val) {},
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          height: double.infinity,
          decoration: const BoxDecoration(
            image: DecorationImage(
              opacity: 0.5,
              image: NetworkImage(
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTczUiJztmsTapvlP8Mzc3hHxxYOo7qALNwvEU8SBwtT8PuC5teqqx28oreisHxapuBRzk&usqp=CAU"),
              fit: BoxFit.cover,
            ),
          ),
          child: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.only(
                  left: 60,
                  right: 60,
                  top: 70,
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Image.network(
                          "https://cdn-icons-png.flaticon.com/128/3408/3408455.png",
                          height: 280,
                          width: 320,
                        ),
                        const SizedBox(height: 10),
                        TextFormField(
                          controller: username,
                          //key: usernameKey,
                          decoration: InputDecoration(
                            hintText: "Enter username",
                            label: Text("Username",
                                style: TextStyle(color: defaultColor)),
                            border: const OutlineInputBorder(
                              borderSide: BorderSide(width: 2),
                              //borderRadius: BorderRadius.circular(20),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide:
                                  BorderSide(color: defaultColor, width: 2),
                            ),
                            prefixIcon: Icon(Icons.person, color: defaultColor),
                          ),
                          validator: (value) {
                            //print("I am in username validatior");
                            if (value == null || value.isEmpty) {
                              return "Please enter username";
                            } else {
                              for (int i = 0; i < validation.length; i++) {
                                //print(validation[i]['name']);
                                if (validation[i]['name'] == username.text) {
                                  return null;
                                }
                              }
                              return "Enter valid username";
                            }
                          },
                          keyboardType: TextInputType.emailAddress,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                            controller: password,
                            //key: passwordKey,
                            obscureText: hidePwd,
                            obscuringCharacter: "*",
                            decoration: InputDecoration(
                                hintText: "Enter password",
                                label: Text("Password",
                                    style: TextStyle(color: defaultColor)),
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide(width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: defaultColor, width: 2),
                                    borderRadius: BorderRadius.circular(20)),
                                prefixIcon: IconButton(
                                  onPressed: () {},
                                  icon: const Icon(Icons.lock),
                                  color: defaultColor,
                                ),
                                suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (hidePwd) {
                                          hidePwd = false;
                                        } else {
                                          hidePwd = true;
                                        }
                                      });
                                    },
                                    icon: const Icon(
                                        Icons.remove_red_eye_outlined),
                                    color: defaultColor)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Please enter password";
                              } else {
                                for (int i = 0; i < validation.length; i++) {
                                  if (validation[i]['name'] == username.text) {
                                    if (validation[i]['pwd'] == password.text) {
                                      return null;
                                    } else {
                                      return "Enter valid  password";
                                    }
                                  }
                                }
                                return "Enter valid password";
                              }
                            }),
                        const SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          style: ButtonStyle(
                              fixedSize:
                                  const MaterialStatePropertyAll(Size(150, 50)),
                              backgroundColor:
                                  MaterialStatePropertyAll(defaultColor)),
                          onPressed: () {
                            //bool loginValidate1 =
                            //  usernameKey.currentState!.validate();
                            // bool loginValidate2 =
                            //   passwordKey.currentState!.validate();
                            bool formValidate =
                                formKey.currentState!.validate();
                            // print(formValidate);

                            if (formValidate) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                backgroundColor: defaultColor,
                                content: const Text("Login Successful"),
                              ));
                              setState(() {
                                isValidate = true;
                              });
                            } else {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                backgroundColor: Colors.red,
                                content: Text("Invalid password or username"),
                              ));
                            }
                          },
                          child: const Text("Login",
                              style:
                                  TextStyle(fontSize: 20, color: Colors.white)),
                        )
                      ],
                    ))),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Page();
  }
}
