// ignore_for_file: non_constant_identifier_names

import 'package:flutter/widgets.dart';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

dynamic database;

class Employee {
  final String name;
  final int empId;
  //final String comp;

  Employee({
    required this.name,
    required this.empId,
    //required this.comp,
  });
  Map<String, dynamic> EmployeeMap() {
    return {'name': name, 'empId': empId};
  }

  @override
  String toString() {
    return '{name:$name , empId:$empId}';
  }
}

Future insertEmployeeData(Employee obj) async {
  final localDB = await database;
  await localDB.insert(
    "Employee",
    obj.EmployeeMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<Employee>> getEmployeeData() async {
  final localDB = await database;
  List<Map<String, dynamic>> listEmployee = await localDB.query("Employee");
  return List.generate(listEmployee.length, (i) {
    return Employee(
      name: listEmployee[i]['name'],
      empId: listEmployee[i]['empId'],
    );
  });
}

Future<void> deleteEmployeeData(int empId) async {
  final localDB = await database;
  await localDB.delete(
    'Employee',
    where: 'empId = ?',
    whereArgs: [empId],
  );
}

Future<void> updateEmployeeData(Employee updatedEmployee) async {
  final localDB = await database;
  await localDB.update(
    'Employee',
    updatedEmployee.EmployeeMap(),
    where: 'empId = ?',
    whereArgs: [updatedEmployee.empId],
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = openDatabase(join(await getDatabasesPath(), "EmployeeDB.db"),
      version: 1, onCreate: (db, version) async {
    await db.execute('''
          CREATE TABLE Employee(
            name TEXT,
            empId INTEGER PRIMARY KEY,
          
          )
        ''');
  });

  //insert into
  Employee emp1 = Employee(name: "Namrata Chaudhari", empId: 1);
  await insertEmployeeData(emp1);
  Employee emp2 = Employee(name: "Nikita Mundphan", empId: 2);
  await insertEmployeeData(emp2);

  print(await getEmployeeData());

  // Delete
  await deleteEmployeeData(1);
  // Update
  Employee updatedEmployee = Employee(name: "Updated Name", empId: 2);
  await updateEmployeeData(updatedEmployee);

  print(await getEmployeeData());
}
