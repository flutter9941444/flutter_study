import 'package:flutter/material.dart';

class Assign4 extends StatelessWidget {
  const Assign4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          border: Border.all(width: 2),
        ),
        child: Row(children: [
          Container(
            padding: const EdgeInsets.all(15),
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.red,
              border: Border.all(width: 2),
            ),
          ),
          const SizedBox(width: 20),
          Container(
              padding: const EdgeInsets.all(15),
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                color: Colors.purple,
                border: Border.all(width: 2),
              ))
        ]),
      ),
    ));
  }
}
