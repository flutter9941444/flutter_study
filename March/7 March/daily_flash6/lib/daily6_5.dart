import 'package:flutter/material.dart';

class Assign5 extends StatefulWidget{
  const Assign5({super.key});
  @override
  State createState()=>_Assign5();
}
class _Assign5 extends State{
  bool color1=true;
  bool color2=true;
  bool color3=true;
  @override 
  Widget build(BuildContext context){
    return Scaffold(
      body:Column(children: [
        widget(
          child: Container(
            height:100,width:200,
            
            color: (color1)?Colors.white:Colors.red,
          ),
        ),
         Container(
          height:100,width:200,
          
          color: (color1)?Colors.white:Colors.pink,
        ),
         Container(
          height:100,width:200,
          
          color: (color1)?Colors.white:Colors.green,
        ),
      ],)
    );
  }
}